open Affine_aeh

module Components = struct
  let formatter = Format.str_formatter

  let no_std = false

  module Parser = Lexer
  module Desugaring = Desugaring
  module Inference = Inference
  module Evaluation = Evaluation
end

module Runner = struct
  include Main.Make (Components)

  let run src =
    run src;
    Format.flush_str_formatter ()
end

let test_literal () =
  Alcotest.(check string) "int 1" "$result : int = 1\n" @@ Runner.run "1";
  Alcotest.(check string) "int 100" "$result : int = 100\n" @@ Runner.run "100";
  Alcotest.(check string) "float 5." "$result : float = 5.\n" @@ Runner.run "5.";
  Alcotest.(check string) "float 0.071" "$result : float = 0.071\n" @@ Runner.run "0.071";
  Alcotest.(check string) "float 14.23" "$result : float = 14.23\n" @@ Runner.run "14.23";
  Alcotest.(check string) "float 1.e3" "$result : float = 1000.\n" @@ Runner.run "1.e3";
  Alcotest.(check string) "float 2.e-2" "$result : float = 0.02\n" @@ Runner.run "2.e-2";
  Alcotest.(check string) "bool true" "$result : bool = true\n" @@ Runner.run "true";
  Alcotest.(check string) "bool true" "$result : bool = false\n" @@ Runner.run "false";
  Alcotest.(check string) "string \"abc\"" "$result : string = \"abc\"\n" @@ Runner.run {|"abc"|}

let test_tuple () =
  Alcotest.(check string) "unit" "$result : unit = ()\n" @@ Runner.run "()";
  Alcotest.(check string) "int pair" "$result : int * int = (1, 2)\n" @@ Runner.run "(1, 2)";
  Alcotest.(check string) "3-tuple" "$result : int * float * bool = (5, 3.4, true)\n"
  @@ Runner.run "(5, 3.4, true)"

let test_list () =
  Alcotest.(check string) "empty list" "$result : 'a list = []\n" @@ Runner.run "[]";
  Alcotest.(check string) "cons" "$result : int list = [1]\n" @@ Runner.run "1 :: []";
  Alcotest.(check string) "cons 2 values" "$result : int list = [1; 2]\n"
  @@ Runner.run "1 :: 2 :: []";
  Alcotest.(check string) "pattern matching for list" "$result : int = 6\n"
  @@ Runner.run
       {| let rec sum = function
            | [] -> 0
            | x :: xs -> x + sum xs
          in sum [1; 2; 3]
       |};
  Alcotest.(check string) "length of list" "$result : int = 3\n" @@ Runner.run "length [1; 2; 3]"

let test_function () =
  Alcotest.(check string)
    "identity function" "$result : 'a -> 'a = <fun>\n" (Runner.run "fun x -> x");
  Alcotest.(check string)
    "" "$result : int * int -> int = <fun>\n"
    (Runner.run "fun (x, y) -> x + y");
  Alcotest.(check string)
    "" "$result : box -> int = <fun>\n"
    (Runner.run "type box = Box of int;; fun (Box n) -> n")

let test_handler () =
  Alcotest.(check string) "" "$result : 'a => 'a = <handler>\n"
  @@ Runner.run "handler effect (Hello _) k -> k ()";
  Alcotest.(check string) "" "$result : 'a => 'a = <handler>\n"
  @@ Runner.run
       {| handler
            | effect (ReturnOne _) k -> k 1
            | effect (ReturnZero _) k -> k 0
       |};
  Alcotest.(check string) "" "$result : 'a => ('b -> 'b) = <handler>\n"
  @@ Runner.run "handler v -> fun x -> x"

let test_effect () =
  Alcotest.(check string) "" "$result : unit = ()\n" (Runner.run "perform Hello");
  Alcotest.(check string) "" "$result : int = 0\n" (Runner.run "perform ReturnZero");
  Alcotest.(check string) "" "$result : int = 1\n" (Runner.run "perform ReturnOne")

let test_simple_comp () =
  Alcotest.(check string) "addition" "$result : int = 7\n" @@ Runner.run "3 + 4";
  Alcotest.(check string) "operator precedence2" "$result : bool = true\n"
  @@ Runner.run "3 * 5 = 10 + 5";
  Alcotest.(check string) "operator precedence1" "$result : int = 23\n" @@ Runner.run "3 + 4 * 5";
  Alcotest.(check string) "application" "$result : int = 6\n" @@ Runner.run "(fun x -> x) 6";
  Alcotest.(check string) "affine application" "$result : int = 10\n"
  @@ Runner.run "(let f x = fun y #-> x +# y in f 2 # 8)";
  Alcotest.(check string) "string composition" "$result : string = \"abcd\"\n"
  @@ Runner.run {|"ab" ^ "cd"|};
  Alcotest.(check string) "invalid comparison" "Runtime Error: invalid comparison\n"
  @@ Runner.run "(+) = (-)";
  Alcotest.(check string) "max" "$result : int = 5\n" @@ Runner.run "max 5 2"

let test_let () =
  Alcotest.(check string) "" "$result : int = 7\n" (Runner.run "let x = 3 in x + 4");
  Alcotest.(check string) "" "$result : int = 15\n" (Runner.run "let f x = x + 3 in f 4 + f 5")

let test_letrec () =
  Alcotest.(check string) "" "$result : int = 120\n"
  @@ Runner.run "let rec fact n = if n = 0 then 1 else n * fact (n - 1) in fact 5";
  Alcotest.(check string)
    "" "val odd : int -> bool = <fun>\nval even : int -> bool = <fun>\n$result : bool = false\n"
  @@ Runner.run
       {|
    let rec odd = function
      | 0 -> false
      | n -> even (n - 1)

    and even = function
      | 0 -> true
      | n -> odd (n - 1);;

    even 15
  |}

let test_typedecl () =
  Alcotest.(check string) "" "$result : t = 1\n" (Runner.run "type t = int;; (1 : t)")

let test_variant () =
  Alcotest.(check string)
    "intuitonistic variant" "$result : box = Box 10\n"
    (Runner.run "type box = Box of int;; Box 10")

let test_poly_variant () =
  Alcotest.(check string)
    "" "$result : int box = Box 10\n"
    (Runner.run "type 'a box = Box of 'a;; Box 10");
  Alcotest.(check string)
    "" "$result : unit box = Box ()\n"
    (Runner.run "type 'a box = Box of 'a;; Box ()")

let test_pattern_match () =
  Alcotest.(check string)
    "" "$result : int = 110\n"
    (Runner.run
       {|
      type 'a option = None | Some of 'a;;
      let unwrap default = function
        | None -> 100
        | Some x -> x
      in unwrap 100 (Some 10) + unwrap 100 None
    |});
  Alcotest.(check string)
    "" "$result : int = 110\n"
    (Runner.run
       {|
      type 'a #option = None | Some of 'a;;
      
      let unwrap default t = match# t with
        | None -> 100
        | Some x -> x
      in unwrap 100 (Some 10) + unwrap 100 None
  |});
  Alcotest.(check string)
    "affine pattern in unrestricted pattern"
    "Type Error: intuitionistic pattern expected, but got affine one\n"
  @@ Runner.run
       {|
         type t1 = Box1 of int;;
         type #t2 = Box2 of t1;;
         type t3 = Box3 of t2;;

         let f = fun x -> match x with
           | Box3 (Box2 (Box1 x)) -> x;;
       |}

let test_option_handler () =
  Alcotest.(check string) "option handler" "$result : int option = Some 14\n"
  @@ Runner.run
       {|
          type 'a option = None | Some of 'a;;

          effect Option : int option -> int;;

          let option = handler
            | effect (Option (Some v)) k -> k v
            | effect (Option None) _ -> None
            | v -> v
          in
          with option handle
            let v = perform (Option (Some 5)) in
            Some (v + 9);;
       |}

let test_result_handler () =
  Alcotest.(check string) "result handler" "$result : (int, string) result = Ok 14\n"
  @@ Runner.run
       {|
          type ('a, 'b) result = Ok of 'a | Error of 'b;;

          effect Result : (int, string) result -> int;;

          let result = handler
            | effect (Result (Ok v)) k -> k v
            | effect (Result (Error e)) k -> Error e
            | v -> v
          in
          with result handle
            let v = perform (Result (Ok 5)) in
            Ok (v + 9);;
       |}

let test_value_restriction () =
  Alcotest.(check string)
    "value restriction" "$result : '_a -> '_a = <fun>\n"
    (Runner.run "(fun _ -> fun x -> x) ()")

(* let test_comment () =
  Alcotest.(check string) "comment" "$result : int = 10\n" (Runner.run "3 + (* 4 + *) 7") *)

let () =
  let open Alcotest in
  run "Affine AEH interpreter"
    [
      ( "value cases",
        [
          test_case "literal" `Quick test_literal;
          test_case "tuple" `Quick test_tuple;
          test_case "list" `Quick test_list;
          test_case "function" `Quick test_function;
          test_case "handler" `Quick test_handler;
        ] );
      ( "computation cases",
        [
          test_case "effect invocation" `Quick test_effect;
          test_case "simple computation" `Quick test_simple_comp;
          test_case "let-in" `Quick test_let;
          test_case "recursive function" `Quick test_letrec;
        ] );
      ( "user defined type",
        [
          test_case "type declaration" `Quick test_typedecl;
          test_case "variant type" `Quick test_variant;
          test_case "polymorphic variant type" `Quick test_poly_variant;
          test_case "pattern matching" `Quick test_pattern_match;
          test_case "option handler" `Quick test_option_handler;
          test_case "result handler" `Quick test_result_handler;
        ] );
      ("polymorphic type", [ test_case "value restriction" `Quick test_value_restriction ]);
      (* ("parsing", [ test_case "comment" `Quick test_comment ]); *)
    ]
