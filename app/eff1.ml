open Affine_aeh

let run path =
  let module Runner = Main.Make (Main.Components) in
  Runner.run_file path

let () = Arg.parse [] run ""
