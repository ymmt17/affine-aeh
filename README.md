# README

2020 年度卒業研究「ワンショット代数効果に対するアフィン型システム」において実装した，限定継続が one shot の algebraic effects を持つ言語の処理系です．

## 開発環境

-   OCaml (4.08.0)
-   Menhir (20190924)

## ビルド

dune でビルドできます．

```
dune build ./src/affine_aeh.cma
```

## 実行

```
cd app
dune exec ./eff1.exe <path>
```

## 構文

Eff (https://www.eff-lang.org) のものと概ね同様です．

## メモ

-   相互再帰関数の記述 (`let rec ... and ...`) はトップレベルの関数定義に対してしか実装していません．
-   末尾に`#`のついた演算子は affine な型を持ちます．たとえば，`(+) : int -> int -> int`に対し，`(+#) : int #-> int #-> int`であり，`+#`は高々 1 回しか使えない変数も受け取ることができます．
