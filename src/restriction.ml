type t =
  [ `Unrestricted
  | `AtMostOnce
  ]

let join t = function
  | `Unrestricted -> `Unrestricted
  | `AtMostOnce -> t
