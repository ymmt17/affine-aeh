open Output
open Symbol
module Sugared = SugaredSyntax
open UntypedSyntax

let ( let* ) = Result.bind

module type S = sig
  val formatter : Format.formatter

  val no_std : bool

  module Parser : sig
    val read_file : string -> (Declaration.t list, Error.t) result

    val read_string : string -> (Declaration.t list, Error.t) result
  end

  module Desugaring : sig
    type state

    val new_state : state

    val desugar_comp :
      state -> ?restr:Restriction.t -> Sugared.Term.t -> (Computation.t * state, Error.t) result

    val desugar_toplevel_let :
      state ->
      Sugared.Pattern.t ->
      Sugared.Term.t ->
      (Pattern.t * Computation.t * state, Error.t) result

    val desugar_toplevel_letrec :
      state ->
      (string * Sugared.Term.t) list ->
      ((Variable.t * Pattern.t * Restriction.t * Computation.t) list * state, Error.t) result

    val desugar_effect :
      state ->
      string ->
      Restriction.t ->
      Sugared.Ty.t ->
      Sugared.Ty.t ->
      (Effect.t * Ty.t * Ty.t * state, Error.t) result

    val desugar_newtype :
      state ->
      restr:Restriction.t ->
      string list ->
      string ->
      Sugared.TyDef.t ->
      (TyName.t * Parameter.t list * TyDef.t * state, Error.t) result
  end

  module Inference : sig
    type state

    val new_state : state

    type context

    val new_context : context

    val add_effect : context -> Effect.t -> Restriction.t -> Ty.t -> Ty.t -> context

    val add_tydef : context -> Parameter.t list -> TyName.t -> TyDef.t -> context

    val infer_top :
      state -> context -> Computation.t -> (Ty.scheme * context * state, Error.t) result

    val infer_toplevel_let :
      state ->
      context ->
      Pattern.t ->
      Computation.t ->
      ((Variable.t * Ty.scheme) list * context * state, Error.t) result

    val infer_toplevel_letrec :
      state ->
      context ->
      (Variable.t * Pattern.t * Restriction.t * Computation.t) list ->
      (Ty.scheme list * context * state, Error.t) result
  end

  module Evaluation : sig
    type state

    val new_state : state

    val run : state -> Computation.t -> (Value.t, Error.t) result

    val eval_toplevel_let :
      state ->
      (Variable.t * Ty.scheme) list ->
      Pattern.t ->
      Computation.t ->
      ((Variable.t * Ty.scheme * Value.t) list * state, Error.t) result

    val eval_toplevel_letrec :
      state ->
      (Ty.scheme * Variable.t * Pattern.t * Computation.t) list ->
      ((Variable.t * Ty.scheme * Value.t) list * state, Error.t) result
  end
end

module Make (M : S) = struct
  open M

  type state = {
    desugar_state : Desugaring.state;
    infer_state : Inference.state;
    type_context : Inference.context;
    eval_state : Evaluation.state;
  }

  let new_state =
    {
      desugar_state = Desugaring.new_state;
      infer_state = Inference.new_state;
      type_context = Inference.new_context;
      eval_state = Evaluation.new_state;
    }

  let process_def ppf state = function
    | Declaration.Term t ->
        let* t, desugar_state = Desugaring.desugar_comp state.desugar_state t in
        let* tysch, type_context, infer_state =
          Inference.infer_top state.infer_state state.type_context t
        in
        let* v = Evaluation.run state.eval_state t in
        Format.fprintf ppf "$result : %t = %t\n%!" (Ty.print tysch) (Value.print v);
        Ok { state with desugar_state; infer_state; type_context }
    | Declaration.Effect (eff, restr, ty1, ty2) ->
        let* eff, ty1, ty2, desugar_state =
          Desugaring.desugar_effect state.desugar_state eff restr ty1 ty2
        in
        let type_context = Inference.add_effect state.type_context eff restr ty1 ty2 in
        Ok { state with desugar_state; type_context }
    | Declaration.TyDef (restr, name, params, decl) ->
        let* name, params, decl, desugar_state =
          Desugaring.desugar_newtype state.desugar_state ~restr params name decl
        in
        let type_context = Inference.add_tydef state.type_context params name decl in
        Ok { state with desugar_state; type_context (*; infer_state *) }
    | Declaration.Let (p, t) ->
        let* p, c, desugar_state = Desugaring.desugar_toplevel_let state.desugar_state p t in
        let* bindings, type_context, infer_state =
          Inference.infer_toplevel_let state.infer_state state.type_context p c
        in
        let* bindings, eval_state = Evaluation.eval_toplevel_let state.eval_state bindings p c in
        ListLabels.iter bindings ~f:(fun (x, tysch, v) ->
            Format.fprintf ppf "val %t : %t = %t\n" (Variable.print x) (Ty.print tysch)
              (Value.print v));
        Ok { desugar_state; infer_state; type_context; eval_state }
    | Declaration.LetRec binds ->
        let* binds, desugar_state = Desugaring.desugar_toplevel_letrec state.desugar_state binds in
        let* tyschs, type_context, infer_state =
          Inference.infer_toplevel_letrec state.infer_state state.type_context binds
        in
        let binds =
          ListLabels.map2 binds tyschs ~f:(fun (x, p, _restr, c) tysch -> (tysch, x, p, c))
        in
        let* binds, eval_state = Evaluation.eval_toplevel_letrec state.eval_state binds in
        ListLabels.iter binds ~f:(fun (x, tysch, v) ->
            Format.fprintf ppf "val %t : %t = %t\n%!" (Variable.print x) (Ty.print tysch)
              (Value.print v));
        (* let* (x, tysch, v), eval_state =
             Evaluation.eval_toplevel_letrec state.eval_state tysch x p c
           in
           Format.fprintf ppf "val %t : %t = %t\n%!" (Variable.print x) (Ty.print tysch)
             (Value.print v); *)
        Ok { desugar_state; infer_state; type_context; eval_state }

  let process_decls ppf state decls =
    ListLabels.fold_left decls ~init:(Ok state) ~f:(fun acc decl ->
        let* state = acc in
        process_def ppf state decl)

  let process_file ?(ppf = formatter) state path =
    let* decls = Parser.read_file path in
    process_decls ppf state decls

  let process_string ?(ppf = formatter) state src =
    let* decls = Parser.read_string src in
    process_decls ppf state decls

  let std_attached =
    let null_out = open_out "/dev/null" in
    Fun.protect ~finally:(fun () -> close_out null_out) @@ fun () ->
    let ppf = Format.formatter_of_out_channel null_out in
    process_file ~ppf new_state Libpath.std

  let exec processor str =
    (let* state = if no_std then Ok new_state else std_attached in
     processor state str)
    |> function
    | Ok _ -> ()
    | Error kind -> Format.fprintf formatter "%t\n%!" (Error.print kind)

  let run = exec process_string

  let run_file = exec process_file
end

module Components = struct
  let formatter = Format.std_formatter

  let no_std = false

  module Parser = Parser
  module Desugaring = Desugaring
  module Inference = Inference
  module Evaluation = Evaluation
end
