open UntypedSyntax
open Symbol
open Output

let ( let* ) = Result.bind

type state = { var_context : Value.t VarContext.t }

let new_state = { var_context = Predefined.var_instance_context }

let rec extend_context ctx = function
  | Pattern.Const cst1, Value.Const cst2 when cst1 = cst2 -> Ok ctx
  | Pattern.Discard, _ -> Ok ctx
  | Pattern.Var x, v -> Ok (VarContext.add x v ctx)
  | Pattern.Tuple ps, Value.Tuple vs when List.length ps = List.length vs ->
      ListLabels.fold_left2 ps vs ~init:(Ok ctx) ~f:(fun acc p v ->
          let* ctx = acc in
          extend_context ctx (p, v))
  | Pattern.Variant (label1, None), Value.Variant (label2, None) when label1 = label2 -> Ok ctx
  | Pattern.Variant (label1, Some p), Value.Variant (label2, Some v) when label1 = label2 ->
      extend_context ctx (p, v)
  | _ -> Error `PatternMatchFailed

let extend state p v =
  let* var_context = extend_context state.var_context (p, v) in
  Ok { var_context }

let rec eval_cont k = function
  | Output.Val v -> k v
  | Output.Eff (eff, v, k') ->
      let k'' v =
        let* output = k' v in
        eval_cont k output
      in
      Ok (Output.Eff (eff, v, k''))

let rec eval_expr state = function
  | Expression.Const cst -> Ok (Value.Const cst)
  | Expression.Var x -> Ok (VarContext.find x state.var_context)
  | Expression.Annotated (e, _) -> eval_expr state e
  | Expression.Variant (label, None) -> Ok (Value.Variant (label, None))
  | Expression.Variant (label, Some e) ->
      let* v = eval_expr state e in
      Ok (Value.Variant (label, Some v))
  | Expression.Handler h ->
      let h = eval_handler state h in
      Ok (Value.Handler h)
  | Expression.Lambda (_, p, c) ->
      let f v =
        let* state = extend state p v in
        eval_comp state c
      in
      Ok (Value.Closure f)
  | Expression.Tuple es ->
      let* vs =
        ListLabels.fold_right es ~init:(Ok []) ~f:(fun e acc ->
            let* vs = acc in
            let* v = eval_expr state e in
            Ok (v :: vs))
      in
      Ok (Value.Tuple vs)

and eval_comp state = function
  | Computation.Value e ->
      let* v = eval_expr state e in
      Ok (Output.Val v)
  | Computation.Apply (_, e1, e2) -> (
      let* v1 = eval_expr state e1 in
      let* v2 = eval_expr state e2 in
      match v1 with
      | Value.Closure f -> f v2
      | _ -> Error `NotFunction )
  | Computation.Effect (eff, e) ->
      let* v = eval_expr state e in
      let k v = Ok (Output.Val v) in
      Ok (Output.Eff (eff, v, k))
  | Computation.Let (_, p, c1, c2) ->
      let* output = eval_comp state c1 in
      let k v =
        let* state = extend state p v in
        eval_comp state c2
      in
      eval_cont k output
  | Computation.LetRec (x, p, _, c1, c2) ->
      let state_ref = ref state in
      let f v =
        let* state = extend !state_ref p v in
        eval_comp state c1
      in
      let* state = extend state (Pattern.Var x) (Value.Closure f) in
      state_ref := state;
      eval_comp state c2
  | Computation.Handle (e, c) -> (
      let* v = eval_expr state e in
      let* output = eval_comp state c in
      match v with
      | Value.Handler h -> h output
      | _ -> Error `NotHandler )
  | Computation.If (cond, thn, els) ->
      let* v = eval_expr state cond in
      let b = Value.to_bool v in
      eval_comp state (if b then thn else els)
  | Computation.Match (_, cond, cases) ->
      let* v = eval_expr state cond in
      eval_match state v cases

and eval_handler state h = function
  | Output.Val v ->
      let p, c = h.val_clause in
      let* state = extend state p v in
      eval_comp state c
  | Output.Eff (eff, v, k) -> (
      match EffContext.find_opt eff h.eff_clauses with
      | None ->
          let k' v =
            let* output = k v in
            eval_handler state h output
          in
          Ok (Output.Eff (eff, v, k'))
      | Some (p, kvar, c) ->
          let k' v =
            let* output = k v in
            eval_handler state h output
          in
          let* state = extend state p v in
          let* state = extend state kvar (Value.Closure k') in
          eval_comp state c )

and eval_match state v = function
  | [] -> Error `PatternMatchFailed
  | (p, c) :: cases -> (
      match extend state p v with
      | Ok state -> eval_comp state c
      | Error `PatternMatchFailed -> eval_match state v cases
      | Error kind -> Error kind )

and toplevel_handle = function
  | Output.Val v -> Ok v
  | Output.Eff (eff, v, k) -> handle_builtin_eff v k eff.annot

and handle_builtin_eff v k = function
  | "Print" ->
      print_string (Value.to_string v);
      let* output = k (Value.Tuple []) in
      toplevel_handle output
  | "Read" ->
      let s = read_line () in
      let* output = k (Value.Const (Const.String s)) in
      toplevel_handle output
  | "RandomInt" ->
      let n = Value.to_int v in
      let* output = k (Value.Const (Const.Integer (Random.int n))) in
      toplevel_handle output
  | "Hello" ->
      Printf.printf "hello, world!\n";
      let* output = k v in
      toplevel_handle output
  | "ReturnZero" ->
      let* output = k (Value.Const (Const.Integer 0)) in
      toplevel_handle output
  | "ReturnOne" ->
      let* output = k (Value.Const (Const.Integer 1)) in
      toplevel_handle output
  | eff -> Error (`UncaughtEffect eff)

let run state c =
  let* output = eval_comp state c in
  toplevel_handle output

let eval_toplevel_let state bindings p c =
  let* v = run state c in
  let* state = extend state p v in
  let bindings = List.map (fun (x, ty) -> (x, ty, VarContext.find x state.var_context)) bindings in
  Ok (bindings, state)

(* let eval_toplevel_letrec state ty x p c =
  let mut_state = ref state in
  let f v =
    let* state = extend !mut_state p v in
    eval_comp state c
  in
  let* state = extend state (Pattern.Var x) (Value.Closure f) in
  mut_state := state;
  Ok ((x, ty, Value.Closure f), state) *)

let eval_toplevel_letrec state binds =
  let mut_state = ref state in
  let* binds, state =
    ListLabels.fold_right binds
      ~init:(Ok ([], state))
      ~f:(fun (tysch, x, p, c) acc ->
        let* binds, state = acc in
        let f v =
          let* state = extend !mut_state p v in
          eval_comp state c
        in
        let* state = extend state (Pattern.Var x) (Value.Closure f) in
        mut_state := state;
        Ok ((x, tysch, Value.Closure f) :: binds, state))
  in
  Ok (binds, state)
