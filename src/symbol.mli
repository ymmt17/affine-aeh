module type S = sig
  type annot

  type t = {
    id : int;
    annot : annot;
  }

  val compare : t -> t -> int

  val init : unit -> annot -> t

  val make : annot -> t

  val print : t -> Format.formatter -> unit
end

module Variable : S with type annot := string

module Effect : S with type annot := string

module TyName : S with type annot := string

module Label : S with type annot := string

module Parameter : sig
  include S with type annot := unit

  val print_weak : is_weak:bool -> t -> Format.formatter -> unit
end

module Context : Map.S with type key = string

module VarContext : Map.S with type key = Variable.t

module EffContext : Map.S with type key = Effect.t

module TyNameContext : Map.S with type key = TyName.t

module LabelContext : Map.S with type key = Label.t
