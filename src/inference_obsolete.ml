(* open UntypedSyntax
open Symbol

let ( let* ) = Result.bind

type context = {
  var_context : Ty.t VarContext.t;
  aff_context : Ty.t VarContext.t;
  eff_context : (Ty.t * Ty.t) EffContext.t;
  tyname_context : (Parameter.t list * TyDef.t) TyNameContext.t;
  constr_context : (TyName.t * Parameter.t list * Ty.t option) LabelContext.t;
}

let new_context =
  {
    var_context = Predefined.var_ty_context;
    aff_context = VarContext.empty;
    eff_context = Predefined.eff_ty_context;
    tyname_context = Predefined.tyname_def_context;
    constr_context = LabelContext.empty;
  }

type state = {
  constraints : (Ty.t * Ty.t) list;
  param_gen : Parameter.generator;
}

let new_state = { constraints = []; param_gen = Predefined.param_gen }

let new_param state =
  let p, param_gen = state.param_gen.generate () in
  (p, { state with param_gen })

let new_param_ty state =
  let p, state = new_param state in
  (Ty.Param p, state)

let add_effect ctx eff ty1 ty2 =
  let eff_context = EffContext.add eff (ty1, ty2) ctx.eff_context in
  { ctx with eff_context }

let add_tydef ctx params tyname = function
  | TyDef.Alias _ as def ->
      let tyname_context =
        TyNameContext.add tyname (params, def) ctx.tyname_context
      in
      { ctx with tyname_context }
  | TyDef.Variant constrs as def ->
      let constr_context =
        LabelContext.fold
          (fun label tyopt context ->
            LabelContext.add label (tyname, params, tyopt) context)
          constrs ctx.constr_context
      in
      let tyname_context =
        TyNameContext.add tyname (params, def) ctx.tyname_context
      in
      { ctx with constr_context; tyname_context }

let add_constraint state ty1 ty2 =
  { state with constraints = (ty1, ty2) :: state.constraints }

module Subst = Map.Make (Parameter)

let rec subst_ty s = function
  | Ty.Basic _ as t -> t
  | Ty.Apply (tys, name) -> Apply (List.map (subst_ty s) tys, name)
  | Ty.Param p as t -> Option.value ~default:t (Subst.find_opt p s)
  | Ty.Arrow (t1, t2) -> Arrow (subst_ty s t1, subst_ty s t2)
  | Ty.ArrowAff (t1, t2) -> ArrowAff (subst_ty s t1, subst_ty s t2)
  | Ty.Handler (t1, t2) -> Handler (subst_ty s t1, subst_ty s t2)
  | Ty.Tuple ts -> Tuple (List.map (subst_ty s) ts)

let subst_tydef s = function
  | TyDef.Alias ty -> TyDef.Alias (subst_ty s ty)
  | TyDef.Variant constrs ->
      let constrs = LabelContext.map (Option.map (subst_ty s)) constrs in
      TyDef.Variant constrs

let add_subst p ty s =
  let s = Subst.map (subst_ty (Subst.singleton p ty)) s in
  Subst.add p ty s

let subst_context ctx subst =
  let var_context = VarContext.map (subst_ty subst) ctx.var_context in
  { ctx with var_context }

let rec unify ctx ty1 ty2 subst =
  let ty1 = subst_ty subst ty1 and ty2 = subst_ty subst ty2 in
  match (ty1, ty2) with
  | ty1, ty2 when ty1 = ty2 -> Ok subst
  | Ty.Param p, ty | ty, Ty.Param p ->
      if Ty.occurs_in p ty then Error Error.CyclicType
      else Ok (add_subst p ty subst)
  | Ty.Apply (tysl, namel), Ty.Apply (tysr, namer)
    when namel = namer && List.length tysl = List.length tysr ->
      ListLabels.fold_left2 tysl tysr ~init:(Ok subst) ~f:(fun acc ty1 ty2 ->
          let* subst = acc in
          unify ctx ty1 ty2 subst)
  | Ty.Apply (tys, name), ty | ty, Ty.Apply (tys, name) ->
      unify_defined_ty ctx tys name ty subst
  | Ty.Arrow (ty1l, ty1r), Ty.Arrow (ty2l, ty2r)
  | Ty.ArrowAff (ty1l, ty1r), Ty.ArrowAff (ty2l, ty2r)
  | Ty.Handler (ty1l, ty1r), Ty.Handler (ty2l, ty2r) ->
      let* subst = unify ctx ty1l ty2l subst in
      unify ctx ty1r ty2r subst
  | Ty.Tuple tysl, Ty.Tuple tysr when List.length tysl = List.length tysr ->
      ListLabels.fold_left2 tysl tysr ~init:(Ok subst) ~f:(fun acc ty1 ty2 ->
          let* subst = acc in
          unify ctx ty1 ty2 subst)
  | ty1, ty2 -> Error (Error.UnificationFailed (ty1, ty2))

and unify_defined_ty ctx tys name ty subst =
  let* tydef = applied_tydef ctx tys name in
  match tydef with
  | TyDef.Alias ty' -> unify ctx ty ty' subst
  | _ -> Error (Error.UnificationFailed (Ty.Apply (tys, name), ty))

and applied_tydef ctx tys name =
  let params, tydef = TyNameContext.find name ctx.tyname_context in
  let nparams = List.length params and nargs = List.length tys in
  if nparams <> nargs then Error (Error.TyConstrArgsMismatch (nparams, nargs))
  else
    let subst = List.fold_right2 Subst.add params tys Subst.empty in
    Ok (subst_tydef subst tydef)

(* and unify_defined_ty ctx _tys name ty subst =
  match TyNameContext.find name ctx.tyname_context with
  | TyDef.Alias ty' -> unify ctx ty ty' subst
  | _ -> assert false *)

let solve state ctx =
  List.fold_left
    (fun subst (ty1, ty2) -> Result.bind subst (unify ctx ty1 ty2))
    (Ok Subst.empty) state.constraints

module Vars = Set.Make (Variable)

let rec vars_expr ?(vars = Vars.empty) = function
  | Expression.Const _ -> vars
  | Expression.Annotated (e, _) -> vars_expr ~vars e
  | Expression.Variant (_, eopt) ->
      Option.fold eopt ~none:vars ~some:(vars_expr ~vars)
  | Expression.Var x -> Vars.add x vars
  | Expression.Lambda (_, c) | Expression.LambdaAff (_, c) -> vars_comp ~vars c
  | Expression.Tuple es -> List.fold_left (fun vars -> vars_expr ~vars) vars es
  | Expression.Handler h ->
      EffContext.fold
        (fun _ (_, _, c) vars -> vars_comp ~vars c)
        h.eff_clauses vars

and vars_comp ?(vars = Vars.empty) = function
  | Computation.Value e -> vars_expr ~vars e
  | Computation.Effect (_, e) -> vars_expr ~vars e
  | Computation.Apply (e1, e2) | Computation.ApplyAff (e1, e2) ->
      vars_expr ~vars:(vars_expr ~vars e1) e2
  | Computation.Let (_, c1, c2) -> vars_comp ~vars:(vars_comp ~vars c1) c2
  | Computation.LetRec (_, _, _, c1, c2) ->
      vars_comp ~vars:(vars_comp ~vars c1) c2
  | Computation.Handle (_, c) -> vars_comp ~vars c
  | Computation.If (cond, thn, els) ->
      let vars = vars_expr ~vars cond in
      vars_comp ~vars:(vars_comp ~vars thn) els
  | Computation.Match (cond, cases) ->
      let vars = vars_expr ~vars cond in
      List.fold_left (fun vars (_, c) -> vars_comp ~vars c) vars cases

(* let disjoint_expr ctx ~hint =
  let vars = vars_expr hint in
  VarContext.partition (fun x _ -> Vars.mem x vars) ctx.aff_context

let disjoint_comp ctx ~hint =
  let vars = vars_comp hint in
  VarContext.partition (fun x _ -> Vars.mem x vars) ctx.aff_context *)

let disjoint_ctx_with_expr ctx ~hint =
  let vars = vars_expr hint in
  let aff1, aff2 =
    VarContext.partition (fun x _ -> Vars.mem x vars) ctx.aff_context
  in
  ({ ctx with aff_context = aff1 }, { ctx with aff_context = aff2 })

let disjoint_ctx_with_comp ctx ~hint =
  let vars = vars_comp hint in
  let aff1, aff2 =
    VarContext.partition (fun x _ -> Vars.mem x vars) ctx.aff_context
  in
  ({ ctx with aff_context = aff1 }, { ctx with aff_context = aff2 })

let extend ctx bind =
  ListLabels.fold_left bind ~init:ctx ~f:(fun ctx (x, ty) ->
      { ctx with var_context = VarContext.add x ty ctx.var_context })

let extend_aff ctx bind =
  ListLabels.fold_left bind ~init:ctx ~f:(fun ctx (x, ty) ->
      { ctx with aff_context = VarContext.add x ty ctx.aff_context })

let infer_variable ctx x =
  match VarContext.find_opt x ctx.var_context with
  | Some ty -> Ok ty
  | None ->
      VarContext.find_opt x ctx.aff_context
      |> Option.to_result ~none:(Error.VariableNotFound x)

let refresh_params state params =
  ListLabels.fold_right params ~init:([], Subst.empty, state)
    ~f:(fun p (ps, subst, state) ->
      let p', state = new_param state in
      let subst = add_subst p (Ty.Param p') subst in
      (p' :: ps, subst, state))

let rec infer_pat state ctx = function
  | Pattern.Const cst -> Ok (Ty.Basic (Const.infer_ty cst), [], state)
  | Pattern.Var x ->
      let ty, state = new_param_ty state in
      Ok (ty, [ (x, ty) ], state)
  | Pattern.Variant (label, popt) -> (
      let tyname, params, tyopt = LabelContext.find label ctx.constr_context in
      let params, subst, state = refresh_params state params in
      let ty = Ty.Apply (List.map (fun p -> Ty.Param p) params, tyname) in
      match (tyopt, popt) with
      | None, None -> Ok (ty, [], state)
      | Some ty', Some p ->
          let ty' = subst_ty subst ty' in
          let* ty'', bind, state = infer_pat state ctx p in
          let state = add_constraint state ty' ty'' in
          Ok (ty, bind, state)
      | _ -> assert false )
  | Pattern.Tuple ps ->
      let* tys, binds, state =
        ListLabels.fold_right ps
          ~init:(Ok ([], [], state))
          ~f:(fun p acc ->
            let* tys, binds, state = acc in
            let* ty, bind, state = infer_pat state ctx p in
            Ok (ty :: tys, bind :: binds, state))
      in
      Ok (Ty.Tuple tys, List.flatten binds, state)

(* let rec infer_pat state ctx = function
  | Pattern.Const cst -> Ok (Ty.Basic (Const.infer_ty cst), state, ctx)
  | Pattern.Var x ->
      let ty1, state = new_param_ty state in
      let var_context = VarContext.add x ty1 ctx.var_context in
      Ok (ty1, state, { ctx with var_context })
  | Pattern.Variant (label, popt) -> (
      let tyname, params, tyopt = LabelContext.find label ctx.constr_context in
      match (tyopt, popt) with
      | None, None ->
          let params, _, state = refresh_params state params in
          let tys = List.map (fun p -> Ty.Param p) params in
          let ty = Ty.Apply (tys, tyname) in
          Ok (ty, state, ctx)
      | Some ty', Some p ->
          let params, subst, state = refresh_params state params in
          let tys = List.map (fun p -> Ty.Param p) params in
          let ty = Ty.Apply (tys, tyname) in
          let ty' = subst_ty subst ty' in
          let* ty'', state, ctx = infer_pat state ctx p in
          let state = add_constraint state ty' ty'' in
          Ok (ty, state, ctx)
      | _ -> assert false )
  (* | Pattern.Variant (label, popt) -> (
      let tyname, params, tyopt = LabelContext.find label ctx.constr_context in
      let ty = Ty.Apply ([], tyname) in
      match (tyopt, popt) with
      | None, None -> Ok (ty, state, ctx)
      | Some ty', Some p ->
          let* ty'', state, ctx = infer_pat state ctx p in
          let state = add_constraint state ty' ty'' in
          Ok (ty, state, ctx)
      | _ -> assert false ) *)
  | Pattern.Tuple ps ->
      let* tys, state, ctx =
        ListLabels.fold_right ps
          ~init:(Ok ([], state, ctx))
          ~f:(fun p acc ->
            let* tys, state, ctx = acc in
            let* ty, state, ctx = infer_pat state ctx p in
            Ok (ty :: tys, state, ctx))
      in
      Ok (Ty.Tuple tys, state, ctx) *)

let rec infer_effect state ctx eff e =
  let ty1, ty2 = EffContext.find eff ctx.eff_context in
  let* ty, state = infer_expr state ctx e in
  let state = add_constraint state ty1 ty in
  Ok (ty2, state)

and infer_variant state ctx label eopt =
  let tyname, params, tyopt = LabelContext.find label ctx.constr_context in
  let params, subst, state = refresh_params state params in
  let tys = List.map (fun p -> Ty.Param p) params in
  let ty = Ty.Apply (tys, tyname) in
  match (tyopt, eopt) with
  | None, None -> Ok (ty, state)
  | Some ty', Some e ->
      let ty' = subst_ty subst ty' in
      let* ty'', state = infer_expr state ctx e in
      let state = add_constraint state ty' ty'' in
      Ok (ty, state)
  | _ -> assert false

and infer_expr state ctx = function
  | Expression.Const cst -> Ok (Ty.Basic (Const.infer_ty cst), state)
  | Expression.Var x ->
      let* ty = infer_variable ctx x in
      Ok (ty, state)
  | Expression.Variant (label, eopt) -> infer_variant state ctx label eopt
  | Expression.Annotated (e, ty) ->
      let* ty', state = infer_expr state ctx e in
      let state = add_constraint state ty ty' in
      Ok (ty, state)
  | Expression.Lambda (p, c) ->
      let* ty1, bind, state = infer_pat state ctx p in
      let ctx = extend ctx bind in
      let* ty2, state = infer_comp state ctx c in
      Ok (Ty.Arrow (ty1, ty2), state)
  | Expression.LambdaAff (x, c) ->
      let ty1, state = new_param_ty state in
      let aff_context = VarContext.add x ty1 ctx.aff_context in
      let* ty2, state = infer_comp state { ctx with aff_context } c in
      Ok (Ty.ArrowAff (ty1, ty2), state)
  | Expression.Tuple es ->
      let* tys, state, _ =
        ListLabels.fold_right es
          ~init:(Ok ([], state, ctx))
          ~f:(fun e acc ->
            let* tys, state, ctx = acc in
            let ctx1, ctx2 = disjoint_ctx_with_expr ctx ~hint:e in
            let* ty, state = infer_expr state ctx1 e in
            Ok (ty :: tys, state, ctx2))
      in
      Ok (Ty.Tuple tys, state)
  | Expression.Handler h ->
      let ty1, state = new_param_ty state in
      let ty2, state = new_param_ty state in
      let x, c = h.val_clause in
      let var_context = VarContext.add x ty1 ctx.var_context in
      let aff_context = VarContext.empty in
      let* ty, state =
        infer_comp state { ctx with var_context; aff_context } c
      in
      let state = add_constraint state ty ty2 in
      let* state =
        EffContext.fold
          (fun eff (x, k, c) acc ->
            let* state = acc in
            let ty1eff, ty2eff = EffContext.find eff ctx.eff_context in
            let var_context = VarContext.add x ty1eff ctx.var_context in
            let aff_context = VarContext.singleton k (Ty.Arrow (ty2eff, ty2)) in
            let* ty, state =
              infer_comp state { ctx with var_context; aff_context } c
            in
            let state = add_constraint state ty ty2 in
            Ok state)
          h.eff_clauses (Ok state)
      in
      Ok (Ty.Handler (ty1, ty2), state)

and infer_comp state ctx = function
  | Computation.Value e -> infer_expr state ctx e
  | Computation.Effect (eff, e) -> infer_effect state ctx eff e
  | Computation.Apply (e1, e2) ->
      let* ty1, state = infer_expr state ctx e1 in
      let aff_context = VarContext.empty in
      let* ty2, state = infer_expr state { ctx with aff_context } e2 in
      let ty, state = new_param_ty state in
      let state = add_constraint state ty1 (Ty.Arrow (ty2, ty)) in
      Ok (ty, state)
  | Computation.ApplyAff (e1, e2) ->
      let ctx1, ctx2 = disjoint_ctx_with_expr ctx ~hint:e1 in
      let* ty1, state = infer_expr state ctx1 e1 in
      let* ty2, state = infer_expr state ctx2 e2 in
      let ty, state = new_param_ty state in
      let state = add_constraint state ty1 (Ty.ArrowAff (ty2, ty)) in
      Ok (ty, state)
  | Computation.Let (p, c1, c2) ->
      let ctx1, ctx2 = disjoint_ctx_with_comp ctx ~hint:c1 in
      let* ty1, state = infer_comp state ctx1 c1 in
      let* ty1', bind, state = infer_pat state ctx p in
      let ctx2 = extend ctx2 bind in
      let state = add_constraint state ty1 ty1' in
      infer_comp state ctx2 c2
  (* | Computation.Let (x, c1, c2) ->
      let aff1, aff2 = disjoint_comp ctx ~hint:c1 in
      let* ty1, state = infer_comp state { ctx with aff_context = aff1 } c1 in
      let* subst = solve state ctx in
      let ctx = subst_context ctx subst in
      let ty1 = subst_ty subst ty1 in
      let var_context = VarContext.add x ty1 ctx.var_context in
      let* ty, state =
        infer_comp state { ctx with var_context; aff_context = aff2 } c2
      in
      Ok (ty, state) *)
  | Computation.LetRec (x, p, m, c1, c2) ->
      let ty1, state = new_param_ty state in
      let ty2, state = new_param_ty state in
      let ctx =
        match m with
        | `Int -> extend ctx [ (x, Ty.Arrow (ty1, ty2)) ]
        | `Aff -> extend ctx [ (x, Ty.ArrowAff (ty1, ty2)) ]
      in
      let ctx1, ctx2 = disjoint_ctx_with_comp ctx ~hint:c1 in
      let* ty1', bind, state = infer_pat state ctx p in
      let ctx1 =
        match m with `Int -> extend ctx1 bind | `Aff -> extend_aff ctx1 bind
      in
      (* let ctx1 = extend ctx1 bind in *)
      let* ty2', state = infer_comp state ctx1 c1 in
      let state = add_constraint state ty1 ty1' in
      let state = add_constraint state ty2 ty2' in
      infer_comp state ctx2 c2
  | Computation.Handle (h, c) ->
      let ctx1, ctx2 = disjoint_ctx_with_expr ctx ~hint:h in
      let* ty1, state = infer_expr state ctx1 h in
      let* ty2, state = infer_comp state ctx2 c in
      let ty, state = new_param_ty state in
      let state = add_constraint state ty1 (Ty.Handler (ty2, ty)) in
      Ok (ty, state)
  | Computation.If (cond, thn, els) ->
      let ctx1, ctx2 = disjoint_ctx_with_expr ctx ~hint:cond in
      let* ty1, state = infer_expr state ctx1 cond in
      let state = add_constraint state ty1 Ty.bool in
      let* ty2, state = infer_comp state ctx2 thn in
      let* ty3, state = infer_comp state ctx2 els in
      let state = add_constraint state ty2 ty3 in
      Ok (ty2, state)
  | Computation.Match (cond, cases) ->
      let ctx1, ctx2 = disjoint_ctx_with_expr ctx ~hint:cond in
      let* ty1, state = infer_expr state ctx1 cond in
      let ty2, state = new_param_ty state in
      let* state =
        ListLabels.fold_left cases ~init:(Ok state) ~f:(fun acc (p, c) ->
            let* state = acc in
            let* ty1', bind, state = infer_pat state ctx2 p in
            let ctx2 = extend ctx2 bind in
            (* let ctx2 = extend_aff ctx2 bind in *)
            let state = add_constraint state ty1 ty1' in
            let* ty2', state = infer_comp state ctx2 c in
            let state = add_constraint state ty2 ty2' in
            Ok state)
      in
      Ok (ty2, state)

and infer_top state ctx c =
  let* ty, state = infer_comp state ctx c in
  let* subst = solve state ctx in
  let ctx = subst_context ctx subst in
  Ok (subst_ty subst ty, state, ctx)

and infer_toplevel_let state ctx p c =
  let* ty1, state = infer_comp state ctx c in
  let* ty2, bind, state = infer_pat state ctx p in
  let ctx = extend ctx bind in
  let state = add_constraint state ty1 ty2 in
  let* subst = solve state ctx in
  let ctx = subst_context ctx subst in
  let bind = List.map (fun (x, ty) -> (x, subst_ty subst ty)) bind in
  Ok (bind, state, ctx)

and infer_toplevel_letrec state ctx x p m c =
  let ty1, state = new_param_ty state in
  let ty2, state = new_param_ty state in
  let ty =
    match m with `Int -> Ty.Arrow (ty1, ty2) | `Aff -> Ty.ArrowAff (ty1, ty2)
  in
  let ctx = extend ctx [ (x, ty) ] in
  let* ty1', bind, state = infer_pat state ctx p in
  let ctx' =
    match m with `Int -> extend ctx bind | `Aff -> extend_aff ctx bind
  in
  let* ty2', state = infer_comp state ctx' c in
  let state = add_constraint state ty1 ty1' in
  let state = add_constraint state ty2 ty2' in
  let* subst = solve state ctx in
  let ctx = subst_context ctx subst in
  Ok (subst_ty subst ty, state, ctx) *)
