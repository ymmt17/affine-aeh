module Sugared = SugaredSyntax
open UntypedSyntax
open Symbol

let ( let* ) = Result.bind

type state = {
  var_context : Variable.t Context.t;
  eff_context : (Effect.t * Restriction.t) Context.t;
  tyname_context : TyName.t Context.t;
  constr_context : (Label.t * Restriction.t * int) Context.t;
}

let new_state =
  {
    var_context = Predefined.var_context;
    eff_context = Predefined.eff_context;
    tyname_context = Predefined.tyname_context;
    constr_context = Predefined.constr_context;
  }

let new_variable state annot =
  let x = Variable.make annot in
  let var_context = Context.add annot x state.var_context in
  (x, { state with var_context })

let new_effect state annot restr =
  let eff = Effect.make annot in
  let eff_context = Context.add annot (eff, restr) state.eff_context in
  (eff, { state with eff_context })

let new_tyname state annot =
  let tyname = TyName.make annot in
  let tyname_context = Context.add annot tyname state.tyname_context in
  (tyname, { state with tyname_context })

let new_constr state annot restr nparams =
  let label = Label.make annot in
  let constr_context = Context.add annot (label, restr, nparams) state.constr_context in
  (label, { state with constr_context })

let new_params state params =
  ListLabels.fold_left params ~init:(Context.empty, state) ~f:(fun (param_context, state) param ->
      let param' = Parameter.make () in
      let param_context = Context.add param param' param_context in
      (param_context, state))

let rec desugar_ty state param_context = function
  | Sugared.Ty.Param p ->
      let* p = Context.find_opt p param_context |> Option.to_result ~none:(`UnknownParameter p) in
      Ok (Ty.Param p, state)
  | Sugared.Ty.Tuple tys ->
      let* tys, state =
        ListLabels.fold_right tys
          ~init:(Ok ([], state))
          ~f:(fun ty acc ->
            let* tys, state = acc in
            let* ty, state = desugar_ty state param_context ty in
            Ok (ty :: tys, state))
      in
      Ok (Ty.Tuple tys, state)
  | Sugared.Ty.Apply (tys, name) ->
      let* tyname =
        Context.find_opt name state.tyname_context |> Option.to_result ~none:(`UnknownType name)
      in
      let* tys, state =
        ListLabels.fold_right tys
          ~init:(Ok ([], state))
          ~f:(fun ty acc ->
            let* tys, state = acc in
            let* ty, state = desugar_ty state param_context ty in
            Ok (ty :: tys, state))
      in
      Ok (Ty.Apply (tys, tyname), state)
  | Sugared.Ty.Arrow (restr, ty1, ty2) ->
      let* ty1, state = desugar_ty state param_context ty1 in
      let* ty2, state = desugar_ty state param_context ty2 in
      Ok (Ty.Arrow (restr, ty1, ty2), state)
  | Sugared.Ty.Handler (ty1, ty2) ->
      let* ty1, state = desugar_ty state param_context ty1 in
      let* ty2, state = desugar_ty state param_context ty2 in
      Ok (Ty.Handler (ty1, ty2), state)

let desugar_tydef state ~restr param_context = function
  | Sugared.TyDef.Alias ty ->
      let* ty, state = desugar_ty state param_context ty in
      Ok (TyDef.Alias ty, state)
  | Sugared.TyDef.Variant constrs ->
      let* constrs, state =
        ListLabels.fold_left constrs
          ~init:(Ok (LabelContext.empty, state))
          ~f:(fun acc (annot, tyopt) ->
            let* constrs, state = acc in
            let* tyopt, nparams, state =
              Option.fold tyopt
                ~none:(Ok (None, 0, state))
                ~some:(function
                  | Sugared.Ty.Tuple tys as ty ->
                      let* ty, state = desugar_ty state param_context ty in
                      Ok (Some ty, List.length tys, state)
                  | ty ->
                      let* ty, state = desugar_ty state param_context ty in
                      Ok (Some ty, 1, state))
            in
            let label, state = new_constr state annot restr nparams in
            Ok (LabelContext.add label (restr, tyopt) constrs, state))
      in
      Ok (TyDef.Variant constrs, state)

let desugar_newtype state ~restr params name def =
  let tyname, state = new_tyname state name in
  let param_context, state = new_params state params in
  let* def, state = desugar_tydef state ~restr param_context def in
  let params = Context.bindings param_context |> List.map snd in
  Ok (tyname, params, def, state)

let desugar_effect state eff restr ty1 ty2 =
  let eff, state = new_effect state eff restr in
  let* ty1, state = desugar_ty state Context.empty ty1 in
  let* ty2, state = desugar_ty state Context.empty ty2 in
  Ok (eff, ty1, ty2, state)

let rec desugar_pat state = function
  | Sugared.Pattern.Const cst -> Ok (Pattern.Const cst, state)
  | Sugared.Pattern.Discard -> Ok (Pattern.Discard, state)
  | Sugared.Pattern.Var x ->
      let x, state = new_variable state x in
      Ok (Pattern.Var x, state)
  | Sugared.Pattern.Variant (label, popt) -> desugar_variant_pat state label popt
  | Sugared.Pattern.Tuple ps ->
      let* ps, state =
        ListLabels.fold_right ps
          ~init:(Ok ([], state))
          ~f:(fun p acc ->
            let* ps, state = acc in
            let* p, state = desugar_pat state p in
            Ok (p :: ps, state))
      in
      Ok (Pattern.Tuple ps, state)

and desugar_variant_pat state label topt =
  let* label, _, nparams =
    Context.find_opt label state.constr_context |> Option.to_result ~none:(`UnknownConstr label)
  in
  match topt with
  | None ->
      if nparams = 0 then Ok (Pattern.Variant (label, None), state)
      else Error (`ConstrArgsMismatch (nparams, 0))
  | Some (Sugared.Pattern.Tuple ps as p) ->
      let nargs = List.length ps in
      if nparams = 1 || nparams = nargs then
        let* p, state = desugar_pat state p in
        Ok (Pattern.Variant (label, Some p), state)
      else Error (`ConstrArgsMismatch (nparams, nargs))
  | Some t ->
      if nparams = 1 then
        let* p, state = desugar_pat state t in
        Ok (Pattern.Variant (label, Some p), state)
      else Error (`ConstrArgsMismatch (nparams, 1))

(* let fold m m' = match (m, m') with `Unrestricted, _ | _, `Unrestricted -> `Unrestricted | _ -> `AtMostOnce *)

let rec desugar_handler state ~restr h =
  let eff_clauses =
    ListLabels.fold_left h.Sugared.Term.eff_clauses ~init:Context.empty
      ~f:(fun bindings (eff, p, k, t) ->
        Context.update eff
          (function
            | None -> Some [ (p, k, t) ]
            | Some cases -> Some ((p, k, t) :: cases))
          bindings)
  in
  let* eff_clauses, state =
    Context.fold
      (fun eff cases acc ->
        let* eff_clauses, state = acc in
        let* eff, restr' =
          Context.find_opt eff state.eff_context |> Option.to_result ~none:(`UnknownEffect eff)
        in
        match cases with
        | [] -> assert false
        | [ (p, k, t) ] ->
            let* p, state = desugar_pat state p in
            let* k, state = desugar_pat state k in
            let* c, state = desugar_comp state ~restr t in
            let eff_clauses = EffContext.add eff (p, k, c) eff_clauses in
            Ok (eff_clauses, state)
        | cases ->
            let xvar, state = new_variable state "$effarg" in
            let kvar, state = new_variable state "$effcont" in
            let* cases, state =
              ListLabels.fold_right cases
                ~init:(Ok ([], state))
                ~f:(fun (p, k, t) acc ->
                  let* cases, state = acc in
                  let* p, state = desugar_pat state p in
                  let* k, state = desugar_pat state k in
                  let* c, state = desugar_comp state ~restr t in
                  let c' =
                    Computation.Let (`AtMostOnce, k, Computation.Value (Expression.Var kvar), c)
                  in
                  Ok ((p, c') :: cases, state))
            in
            let eff_clauses =
              EffContext.add eff
                ( Pattern.Var xvar,
                  Pattern.Var kvar,
                  Computation.Match (restr', Expression.Var xvar, cases) )
                eff_clauses
            in
            Ok (eff_clauses, state))
      eff_clauses
      (Ok (EffContext.empty, state))
  in
  let* val_clause, state =
    let p, t = h.val_clause in
    let* p, state = desugar_pat state p in
    let* c, state = desugar_comp state ~restr t in
    Ok ((p, c), state)
  in
  Ok (eff_clauses, val_clause, state)

(*

type t = Empty | Single of int | Multi of int * int * int

Empty => OK (0 parameters, 0 arguments)
Empty 1 => Error (0 parameters, some arguments)
Single => Error (1 parameter, 0 arguments)
Single 1 => OK (1 parameter, 1 argument)
Single (1, 2) => OK (1 parameter, 1 argument as tuple)
Multi 1 => Error (3 parameters, 1 argument)
Multi (1, 2) => Error (3 parameters, 2 arguments)
Multi (1, 2, 3) => OK (3 parameters, 3 arguments)
Multi (1, 2, 3, 4) => Error (3 parameters, 4 arguments)

*)
and desugar_variant state ~restr:_restr label topt =
  let* label, restr, nparams =
    Context.find_opt label state.constr_context |> Option.to_result ~none:(`UnknownConstr label)
  in
  (* let restr =
       match restr' with
       | `Unrestricted -> `Unrestricted
       | `AtMostOnce -> restr
     in *)
  match topt with
  | None ->
      if nparams = 0 then Ok (Expression.Variant (label, None), [], state)
      else Error (`ConstrArgsMismatch (nparams, 0))
  | Some (Sugared.Term.Tuple ts as t) ->
      let nargs = List.length ts in
      if nparams = 1 || nparams = nargs then
        let* e, bind, state = desugar_expr state ~restr t in
        Ok (Expression.Variant (label, Some e), bind, state)
      else Error (`ConstrArgsMismatch (nparams, nargs))
  | Some t ->
      if nparams = 1 then
        let* e, bind, state = desugar_expr state ~restr t in
        Ok (Expression.Variant (label, Some e), bind, state)
      else Error (`ConstrArgsMismatch (nparams, 1))

and desugar_expr state ~restr = function
  | Sugared.Term.Const cst -> Ok (Expression.Const cst, [], state)
  | Sugared.Term.Var x ->
      let* x =
        Context.find_opt x state.var_context |> Option.to_result ~none:(`UnknownVariable x)
      in
      Ok (Expression.Var x, [], state)
  | Sugared.Term.Annotated (t, ty) ->
      let* e, bind, state = desugar_expr state ~restr t in
      let* ty, state = desugar_ty state Context.empty ty in
      Ok (Expression.Annotated (e, ty), bind, state)
  | Sugared.Term.Lambda (restr', p, t) ->
      let var_old = state.var_context in
      let* p, state = desugar_pat state p in
      let* c, state = desugar_comp state ~restr t in
      Ok (Expression.Lambda (restr', p, c), [], { state with var_context = var_old })
  | Sugared.Term.Variant (label, topt) -> desugar_variant state ~restr label topt
  | Sugared.Term.Tuple ts ->
      let* es, binds, state =
        ListLabels.fold_right ts
          ~init:(Ok ([], [], state))
          ~f:(fun t acc ->
            let* es, binds, state = acc in
            let* e, bind, state = desugar_expr state ~restr t in
            Ok (e :: es, bind :: binds, state))
      in
      Ok (Expression.Tuple es, List.flatten binds, state)
  | Sugared.Term.Handler h ->
      let* eff_clauses, val_clause, state = desugar_handler state ~restr h in
      Ok (Expression.Handler { eff_clauses; val_clause }, [], state)
  | Sugared.Term.Function cases ->
      let x, state = new_variable state "$match" in
      let* cases, state =
        ListLabels.fold_right cases
          ~init:(Ok ([], state))
          ~f:(fun (p, c) acc ->
            let* cases, state = acc in
            let* p, state = desugar_pat state p in
            let* c, state = desugar_comp state ~restr c in
            Ok ((p, c) :: cases, state))
      in
      let c = Computation.Match (`Unrestricted, Expression.Var x, cases) in
      Ok (Expression.Lambda (`Unrestricted, Pattern.Var x, c), [], state)
  | t ->
      let x, state = new_variable state "$tmp" in
      let* c, state = desugar_comp state ~restr t in
      Ok (Expression.Var x, [ (x, restr, c) ], state)

and desugar_comp state ?(restr = `AtMostOnce) = function
  | Sugared.Term.Effect (eff, t) ->
      let* eff, restr' =
        Context.find_opt eff state.eff_context |> Option.to_result ~none:(`UnknownEffect eff)
      in
      let* e, bind, state = desugar_expr state ~restr:(Restriction.join restr restr') t in
      lift_bindings state (Computation.Effect (eff, e)) bind
  | Sugared.Term.Apply (restr', t1, t2) ->
      let* e1, bind1, state = desugar_expr state ~restr t1 in
      let restr = if restr' = `Unrestricted then `Unrestricted else restr in
      let* e2, bind2, state = desugar_expr state ~restr t2 in
      lift_bindings state (Computation.Apply (restr', e1, e2)) (bind1 @ bind2)
  | Sugared.Term.Let (restr', p, t1, t2) ->
      let var_old = state.var_context in
      let restr'' = if restr' = `Unrestricted then `Unrestricted else restr in
      let* c1, state = desugar_comp state ~restr:restr'' t1 in
      let* p, state = desugar_pat state p in
      let* c2, state = desugar_comp state ~restr t2 in
      Ok (Computation.Let (restr', p, c1, c2), { state with var_context = var_old })
  | Sugared.Term.LetRec (x, t1, t2) ->
      let var_old = state.var_context in
      let* restr, p, t1 =
        match t1 with
        | Sugared.Term.Lambda (restr, p, t) -> Ok (restr, p, t)
        | Sugared.Term.Function cases ->
            let t = Sugared.Term.Match (`Unrestricted, Sugared.Term.Var "$match", cases) in
            Ok (`Unrestricted, Sugared.Pattern.Var "$match", t)
        | _ -> Error `IllegalRecursion
      in
      let x, state = new_variable state x in
      let var_old' = state.var_context in
      let* p, state = desugar_pat state p in
      let* c1, state = desugar_comp state ~restr t1 in
      let state = { state with var_context = var_old' } in
      let* c2, state = desugar_comp state ~restr t2 in
      Ok (Computation.LetRec (x, p, restr, c1, c2), { state with var_context = var_old })
  | Sugared.Term.Handle (t1, t2) ->
      let* e, bind, state = desugar_expr state ~restr t1 in
      let* c, state = desugar_comp state ~restr t2 in
      lift_bindings state (Computation.Handle (e, c)) bind
  | Sugared.Term.If (cond, thn, els) ->
      let* cond, bind, state = desugar_expr state ~restr cond in
      let* thn, state = desugar_comp state ~restr thn in
      let* els, state = desugar_comp state ~restr els in
      lift_bindings state (Computation.If (cond, thn, els)) bind
  | Sugared.Term.Match (restr', cond, cases) ->
      let* cond, bind, state = desugar_expr state ~restr:(Restriction.join restr restr') cond in
      let* cases, state =
        ListLabels.fold_right cases
          ~init:(Ok ([], state))
          ~f:(fun (p, c) acc ->
            let* cases, state = acc in
            let* p, state = desugar_pat state p in
            let* c, state = desugar_comp state ~restr c in
            Ok ((p, c) :: cases, state))
      in
      lift_bindings state (Computation.Match (restr', cond, cases)) bind
  | t ->
      let* e, binds, state = desugar_expr state ~restr t in
      lift_bindings state (Computation.Value e) binds

and desugar_toplevel_let state p t =
  (* let var_old = state.var_context in *)
  let* c, state = desugar_comp state t in
  let* p, state = desugar_pat state p in
  Ok (p, c, state)

(* and desugar_toplevel_letrec state x t =
  let* restr, p, t =
    match t with
    | Sugared.Term.Lambda (restr, p, t) -> Ok (restr, p, t)
    | Sugared.Term.Function cases ->
        let t = Sugared.Term.Match (`Unrestricted, Sugared.Term.Var "$match", cases) in
        Ok (`Unrestricted, Sugared.Pattern.Var "$match", t)
    | _ -> Error `IllegalRecursion
  in
  let x, state = new_variable state x in
  let* p, state = desugar_pat state p in
  let* c, state = desugar_comp state t in
  Ok ((x, p, restr, c), state) *)
and desugar_toplevel_letrec state binds =
  let xs, ts = List.split binds in
  let xs, state =
    ListLabels.fold_right xs ~init:([], state) ~f:(fun x (xs, state) ->
        let x, state = new_variable state x in
        (x :: xs, state))
  in
  let* pcs, state =
    ListLabels.fold_right ts
      ~init:(Ok ([], state))
      ~f:(fun t acc ->
        let* pcs, state = acc in
        let* restr, p, t =
          let open Sugared in
          match t with
          | Term.Lambda (restr, p, t) -> Ok (restr, p, t)
          | Term.Function cases ->
              let t = Term.Match (`Unrestricted, Term.Var "$match", cases) in
              Ok (`Unrestricted, Pattern.Var "$match", t)
          | _ -> Error `IllegalRecursion
        in
        let* p, state = desugar_pat state p in
        let* c, state = desugar_comp state t in
        Ok ((restr, p, c) :: pcs, state))
  in
  let binds = ListLabels.map2 xs pcs ~f:(fun x (restr, p, c) -> (x, p, restr, c)) in
  Ok (binds, state)

and lift_bindings state body = function
  | [] -> Ok (body, state)
  | (x, restr, c) :: binds ->
      let* c', state = lift_bindings state body binds in
      Ok (Computation.Let (restr, Pattern.Var x, c, c'), state)
