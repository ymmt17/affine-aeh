open SugaredSyntax

type t =
  | Term of Term.t
  | Effect of string * Restriction.t * Ty.t * Ty.t
  | TyDef of Restriction.t * string * string list * TyDef.t
  | Let of Pattern.t * Term.t
  | LetRec of (string * Term.t) list
