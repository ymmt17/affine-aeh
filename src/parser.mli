val read_string : string -> (Declaration.t list, Error.t) result

val read_file : string -> (Declaration.t list, Error.t) result
