{
  open MenhirParser

  exception UnterminatedComment
}

let operator_char = ['#' '.' '=' '<' '>']

let float_literal =
  '-'? ['0'-'9']+
    ( '.' ['0'-'9']* (['e' 'E'] ['+' '-']? ['0'-'9']+)?
    | ('.' ['0'-'9']*)? (['e' 'E'] ['+' '-']? ['0'-'9']+))

let lowercase_ident = ['a'-'z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_' '\'']*

rule tokenize = parse
  | [' ' '\t' '\r']
      { tokenize lexbuf }
  | "(*"
      { comment 0 lexbuf }
  | '\n'
      { Lexing.new_line lexbuf; tokenize lexbuf }
  | eof
      { EOF }
  | '-'? ['0'-'9']+ as lit
      { INT (int_of_string lit) }
  | float_literal as lit
      { FLOAT (float_of_string lit) }
  | '"' ([^ '"']* as lit) '"'
      { STRING lit }
  | "and"
      { AND }
  | "effect"
      { EFFECT }
  | "else"
      { ELSE }
  | "false"
      { BOOL false }
  | "fun"
      { FUN }
  | "function"
      { FUNCTION }
  | "handle"
      { HANDLE }
  | "handler"
      { HANDLER }
  | "if"
      { IF }
  | "in"
      { IN }
  | "let#"
      { LET1 }
  | "let"
      { LET }
  | "match#"
      { MATCH1 }
  | "match"
      { MATCH }
  | "of"
      { OF }
  | "perform"
      { PERFORM }
  | "rec"
      { REC }
  | "then"
      { THEN }
  | "true"
      { BOOL true }
  | "type"
      { TYPE }
  | "with"
      { WITH }
  | lowercase_ident as lit
      { LOWERCASE_IDENT lit }
  | ['A'-'Z'] ['a'-'z' 'A'-'Z' '0'-'9' '_' '\'']* as lit
      { UPPERCASE_IDENT lit }
  | '\'' lowercase_ident as lit
      { TYPARAM lit }
  | '('
      { LPAREN }
  | ')'
      { RPAREN }
  | '['
      { LBRACK }
  | ']'
      { RBRACK }
  | '_'
      { UNDERSCORE }
  | '#'
      { HASH }
  | ','
      { COMMA }
  | "::"
      { COLONCOLON }
  | ':'
      { COLON }
  | ";;"
      { SEMISEMI }
  | ';'
      { SEMI }
  | '|'
      { BAR }
  | "->"
      { ARROW }
  | "#->"
      { ARROW1 }
  | "=>"
      { DARROW }
  | '*'
      { STAR }
  | ['*' '/'] operator_char* as op
      { INFIXOP_MUL op }
  | ['+' '-'] operator_char* as op
      { INFIXOP_ADD op }
  | ['^' '@'] operator_char* as op
      { INFIXOP_APPEND op }
  | '='
      { EQUAL }
  | ['=' '<' '>'] operator_char* as op
      { INFIXOP_CMP op }

and comment level = parse
  | "*)"
      { if level = 0 then tokenize lexbuf else comment (level - 1) lexbuf }
  | "(*"
      { comment (level + 1) lexbuf }
  | '\n'
      { Lexing.new_line lexbuf; comment level lexbuf }
  | _
      { comment level lexbuf }
  | eof
      { raise UnterminatedComment }

{
  let parse lexbuf =
    try Ok (MenhirParser.decls tokenize lexbuf) with
    | MenhirParser.Error -> Error (`ParseFailed "")
    | UnterminatedComment -> Error `UnterminatedComment
    | Failure _msg -> Error `UnrecognizedToken

  let read_string src =
    let lexbuf = Lexing.from_string src in
    parse lexbuf

  let read_file path =
    let in_channel = open_in path in
    let lexbuf = Lexing.from_channel in_channel in
    let result = parse lexbuf in
    close_in in_channel;
    result
}
