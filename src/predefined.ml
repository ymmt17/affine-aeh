open Output
open Symbol
open UntypedSyntax

let ty_intint restr = Ty.(Scheme ([], Arrow (restr, int, Arrow (restr, int, int))))

let ty_floatfloat restr = Ty.(Scheme ([], Arrow (restr, float, Arrow (restr, float, float))))

let ty_strstr restr = Ty.(Scheme ([], Arrow (restr, string, Arrow (restr, string, string))))

let ty_comparison restr =
  let p = Parameter.make () in
  Ty.(Scheme ([ p ], Arrow (restr, Param p, Arrow (restr, Param p, bool))))

let predefined_ty =
  [
    ("int", TyDef.Alias Ty.int);
    ("bool", TyDef.Alias Ty.bool);
    ("float", TyDef.Alias Ty.float);
    ("string", TyDef.Alias Ty.string);
    ("unit", TyDef.Alias (Ty.Tuple []));
  ]

let tyname_context, tydef_context =
  ListLabels.fold_left predefined_ty ~init:(Context.empty, TyNameContext.empty)
    ~f:(fun (context, tydef_context) (annot, tydef) ->
      let tyname = TyName.make annot in
      let context = Context.add annot tyname context in
      let tydef_context = TyNameContext.add tyname ([], tydef) tydef_context in
      (context, tydef_context))

type datatype = {
  datatype_name : string;
  tyname : TyName.t;
  params : Parameter.t list;
  constrs : constructor list;
}

and constructor = {
  constr_name : string;
  restr : Restriction.t;
  nargs : int;
  tyopt : Ty.t option;
}

let datatype_list =
  let self = TyName.make "list" in
  let p = Parameter.make () in
  {
    datatype_name = "list";
    tyname = self;
    params = [ p ];
    constrs =
      [
        { constr_name = "$nil"; restr = `Unrestricted; nargs = 0; tyopt = None };
        {
          constr_name = "$cons";
          restr = `Unrestricted;
          nargs = 2;
          tyopt = Some Ty.(Tuple [ Param p; Apply ([ Param p ], self) ]);
        };
      ];
  }

let datatype_list1 =
  let self = TyName.make "list1" in
  let p = Parameter.make () in
  {
    datatype_name = "list1";
    tyname = self;
    params = [ p ];
    constrs =
      [
        { constr_name = "$nil1"; restr = `AtMostOnce; nargs = 0; tyopt = None };
        {
          constr_name = "$cons1";
          restr = `AtMostOnce;
          nargs = 2;
          tyopt = Some Ty.(Tuple [ Param p; Apply ([ Param p ], self) ]);
        };
      ];
  }

let datatype_option =
  let p = Parameter.make () in
  {
    datatype_name = "option";
    tyname = TyName.make "option";
    params = [ p ];
    constrs =
      [
        { constr_name = "None"; restr = `Unrestricted; nargs = 0; tyopt = None };
        { constr_name = "Some"; restr = `Unrestricted; nargs = 1; tyopt = Some (Ty.Param p) };
      ];
  }

let predefined_datatype = [ datatype_list; datatype_list1; datatype_option ]

let tyname_context, tydef_context, constr_context, constr_ty_context =
  ListLabels.fold_left predefined_datatype
    ~init:(tyname_context, tydef_context, Context.empty, LabelContext.empty)
    ~f:(fun (tyname_context, tydef_context, constr_context, constr_ty_context)
            { datatype_name; tyname; params; constrs }
            ->
      let tyname_context = Context.add datatype_name tyname tyname_context in
      let constrs, constr_context, constr_ty_context =
        ListLabels.fold_left constrs ~init:(LabelContext.empty, constr_context, constr_ty_context)
          ~f:(fun (constrs, constr_context, constr_ty_context)
                  { constr_name; restr; nargs; tyopt }
                  ->
            let label = Label.make constr_name in
            let constr_context = Context.add constr_name (label, restr, nargs) constr_context in
            let constrs = LabelContext.add label (restr, tyopt) constrs in
            let constr_ty_context =
              LabelContext.add label (tyname, params, restr, tyopt) constr_ty_context
            in
            (constrs, constr_context, constr_ty_context))
      in

      let tydef_context = TyNameContext.add tyname (params, TyDef.Variant constrs) tydef_context in
      (tyname_context, tydef_context, constr_context, constr_ty_context))

let predefined_eff =
  [
    ("Hello", `Unrestricted, Ty.unit, Ty.unit);
    ("Read", `Unrestricted, Ty.unit, Ty.string);
    ("ReturnZero", `Unrestricted, Ty.unit, Ty.int);
    ("ReturnOne", `Unrestricted, Ty.unit, Ty.int);
    ("Print", `Unrestricted, Ty.string, Ty.unit);
    ("RandomInt", `Unrestricted, Ty.int, Ty.int);
  ]

let eff_context, eff_ty_context =
  ListLabels.fold_left predefined_eff ~init:(Context.empty, EffContext.empty)
    ~f:(fun (context, ty_context) (annot, restr, ty1, ty2) ->
      let eff = Effect.make annot in
      let context = Context.add annot (eff, restr) context in
      let ty_context = EffContext.add eff (restr, ty1, ty2) ty_context in
      (context, ty_context))

let to_int = function
  | Value.Const (Const.Integer n) -> n
  | _ -> assert false

let from_int n = Value.Const (Const.Integer n)

let to_float = function
  | Value.Const (Const.Float f) -> f
  | _ -> assert false

let from_float f = Value.Const (Const.Float f)

let to_bool = function
  | Value.Const (Const.Boolean b) -> b
  | _ -> assert false

let from_bool b = Value.Const (Const.Boolean b)

let to_string = function
  | Value.Const (Const.String s) -> s
  | _ -> assert false

let from_string s = Value.Const (Const.String s)

let return v = Ok (Output.Val v)

let closure f = Value.Closure (fun v -> f v)

let closure2 f = Value.Closure (fun v1 -> return @@ Value.Closure (fun v2 -> f v1 v2))

let add_int = closure2 (fun v1 v2 -> return @@ from_int (to_int v1 + to_int v2))

let sub_int = closure2 (fun v1 v2 -> return @@ from_int (to_int v1 - to_int v2))

let mul_int = closure2 (fun v1 v2 -> return @@ from_int (to_int v1 * to_int v2))

let div_int = closure2 (fun v1 v2 -> return @@ from_int (to_int v1 / to_int v2))

let add_float = closure2 (fun v1 v2 -> return @@ from_float (to_float v1 +. to_float v2))

let sub_float = closure2 (fun v1 v2 -> return @@ from_float (to_float v1 -. to_float v2))

let mul_float = closure2 (fun v1 v2 -> return @@ from_float (to_float v1 *. to_float v2))

let div_float = closure2 (fun v1 v2 -> return @@ from_float (to_float v1 /. to_float v2))

let concat_str = closure2 (fun v1 v2 -> return @@ from_string (to_string v1 ^ to_string v2))

let not_bool = closure (fun v -> return @@ from_bool (not @@ to_bool v))

let ( let* ) = Result.bind

module Ord = struct
  type t =
    | Equal
    | Less
    | Greater

  let to_ord n = if n < 0 then Less else if n > 0 then Greater else Equal

  let compare_const cst1 cst2 =
    match (cst1, cst2) with
    | Const.Integer n1, Const.Integer n2 -> Ok (Stdlib.compare n1 n2)
    | Const.Float f1, Const.Float f2 -> Ok (Stdlib.compare f1 f2)
    | Const.Boolean b1, Const.Boolean b2 -> Ok (Stdlib.compare b1 b2)
    | Const.String s1, Const.String s2 -> Ok (Stdlib.compare s1 s2)
    | _ -> Error `InvalidComparison

  let rec compare_list vs1 vs2 =
    match (vs1, vs2) with
    | [], [] -> Ok Equal
    | [], _ -> Ok Less
    | _, [] -> Ok Greater
    | v1 :: vs1, v2 :: vs2 ->
        let* ord = compare v1 v2 in
        if ord = Equal then compare_list vs1 vs2 else Ok ord

  and compare_opt vopt1 vopt2 =
    match (vopt1, vopt2) with
    | Some v1, Some v2 -> compare v1 v2
    | None, None -> Ok Equal
    | None, Some _ -> Ok Less
    | Some _, None -> Ok Greater

  and compare v1 v2 =
    match (v1, v2) with
    | Value.Const cst1, Value.Const cst2 ->
        let* n = compare_const cst1 cst2 in
        Ok (to_ord n)
    | Value.Tuple vs1, Value.Tuple vs2 -> compare_list vs1 vs2
    | Value.Variant (label1, vopt1), Value.Variant (label2, vopt2) ->
        let n = Stdlib.compare label1 label2 in
        if n = 0 then compare_opt vopt1 vopt2 else Ok (to_ord n)
    | _ -> Error `InvalidComparison
end

let compare pred v1 v2 =
  let* ord = Ord.compare v1 v2 in
  return @@ from_bool (pred ord)

let equal = closure2 (compare (( = ) Ord.Equal))

let less_than = closure2 (compare (( = ) Ord.Less))

let greater_than = closure2 (compare (( = ) Ord.Greater))

let float_of_int = closure (fun v -> return @@ from_float @@ float_of_int @@ to_int v)

let string_of_int = closure (fun v -> return @@ from_string @@ string_of_int @@ to_int v)

let int_of_string =
  closure (fun v ->
      try
        let label, _, _ = Context.find "Some" constr_context in
        let v = from_int @@ int_of_string @@ to_string v in
        return @@ Value.Variant (label, Some v)
      with Failure _ ->
        let label, _, _ = Context.find "None" constr_context in
        return @@ Value.Variant (label, None))

let option_sym = Context.find "option" tyname_context

let predefined_var =
  [
    ("+", ty_intint `Unrestricted, add_int);
    ("-", ty_intint `Unrestricted, sub_int);
    ("*", ty_intint `Unrestricted, mul_int);
    ("/", ty_intint `Unrestricted, div_int);
    ("+.", ty_floatfloat `Unrestricted, add_float);
    ("-.", ty_floatfloat `Unrestricted, sub_float);
    ("*.", ty_floatfloat `Unrestricted, mul_float);
    ("/.", ty_floatfloat `Unrestricted, div_float);
    ("=", ty_comparison `Unrestricted, equal);
    ("<", ty_comparison `Unrestricted, less_than);
    (">", ty_comparison `Unrestricted, greater_than);
    ("^", ty_strstr `Unrestricted, concat_str);
    ("+#", ty_intint `AtMostOnce, add_int);
    ("-#", ty_intint `AtMostOnce, sub_int);
    ("*#", ty_intint `AtMostOnce, mul_int);
    ("/#", ty_intint `AtMostOnce, div_int);
    ("=#", ty_comparison `AtMostOnce, equal);
    ("<#", ty_comparison `AtMostOnce, less_than);
    (">#", ty_comparison `AtMostOnce, greater_than);
    ("^#", ty_strstr `AtMostOnce, concat_str);
    ("not", Ty.(Scheme ([], Arrow (`Unrestricted, bool, bool))), not_bool);
    ("float_of_int", Ty.(Scheme ([], Arrow (`Unrestricted, int, float))), float_of_int);
    ("string_of_int", Ty.(Scheme ([], Arrow (`Unrestricted, int, string))), string_of_int);
    ( "int_of_string",
      Ty.(Scheme ([], Arrow (`Unrestricted, string, Apply ([ int ], option_sym)))),
      int_of_string );
  ]

let var_context, var_ty_context, var_instance_context =
  ListLabels.fold_left predefined_var ~init:(Context.empty, VarContext.empty, VarContext.empty)
    ~f:(fun (context, ty_context, instance_context) (annot, ty, v) ->
      let x = Variable.make annot in
      let context = Context.add annot x context in
      let ty_context = VarContext.add x ty ty_context in
      let instance_context = VarContext.add x v instance_context in
      (context, ty_context, instance_context))
