open Symbol

type parse_error =
  [ `ParseFailed of string
  | `UnrecognizedToken
  | `UnterminatedComment
  ]

type desugar_error =
  [ `UnknownVariable of string
  | `UnknownEffect of string
  | `UnknownType of string
  | `UnknownConstr of string
  | `UnknownParameter of string
  | `ConstrArgsMismatch of int * int
  | `IllegalRecursion
  ]

type type_error =
  [ `CyclicType
  | `UnificationFailed of Ty.t * Ty.t
  | `InconsistentRestriction
  | `VariableNotFound of Variable.t
  | `VariableUsedTwice of Variable.t
  | `TyConstrArgsMismatch of int * int
  ]

type runtime_error =
  [ `UncaughtEffect of string
  | `NotFunction
  | `NotHandler
  | `PatternMatchFailed
  | `InvalidComparison
  ]

type t =
  [ parse_error
  | desugar_error
  | type_error
  | runtime_error
  ]

let print t ppf =
  match t with
  | `ParseFailed msg -> Format.fprintf ppf "Parse Error %s" msg
  | `UnrecognizedToken -> Format.fprintf ppf "Parse Error: unrecognized token"
  | `UnterminatedComment -> Format.fprintf ppf "Parse Error: comment is unterminated"
  | `UnknownEffect eff -> Format.fprintf ppf "Error: unknown effect %s" eff
  | `UnknownVariable x -> Format.fprintf ppf "Error: unknown variable %s" x
  | `UnknownType tyname -> Format.fprintf ppf "Error: unknown type %s" tyname
  | `UnknownConstr label -> Format.fprintf ppf "Error: unknown constructor %s" label
  | `UnknownParameter p -> Format.fprintf ppf "Error: unknown parameter %s" p
  | `ConstrArgsMismatch (expected, actual) ->
      Format.fprintf ppf
        "Error: The constructor expects %d argument(s), but is applied here to %d argument(s)"
        expected actual
  | `IllegalRecursion ->
      Format.fprintf ppf
        "Error: This kind of expression is not allowed as right-hand side of `let rec`"
  | `CyclicType -> Format.fprintf ppf "Type Error: cyclic type"
  | `UnificationFailed (ty1, ty2) ->
      Format.fprintf ppf "Type Error: %t expected, but got %t" (Ty.print_ty ty1) (Ty.print_ty ty2)
  | `InconsistentRestriction ->
      Format.fprintf ppf "Type Error: intuitionistic pattern expected, but got affine one"
  | `VariableUsedTwice x ->
      Format.fprintf ppf "Type Error: %t must be used at most once" (Variable.print x)
  | `VariableNotFound x ->
      Format.fprintf ppf "Type Error: %t was not found in context" (Variable.print x)
  | `TyConstrArgsMismatch (expected, actual) ->
      Format.fprintf ppf
        "Type Error: The type constructor expects %d argument(s), but is applied here to %d \
         argument(s)"
        expected actual
  | `UncaughtEffect eff -> Format.fprintf ppf "Runtime Error: uncaught effect %s" eff
  | `NotFunction -> Format.fprintf ppf "Runtime Error: not a function"
  | `NotHandler -> Format.fprintf ppf "Runtime Error: not a handler"
  | `PatternMatchFailed -> Format.fprintf ppf "Runtime Error: pattern match failed"
  | `InvalidComparison -> Format.fprintf ppf "Runtime Error: invalid comparison"
