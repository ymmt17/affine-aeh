open Symbol
open UntypedSyntax

type state

val new_state : state

type context

val new_context : context

val add_effect : context -> Effect.t -> Restriction.t -> Ty.t -> Ty.t -> context

val add_tydef : context -> Parameter.t list -> TyName.t -> TyDef.t -> context

val infer_top : state -> context -> Computation.t -> (Ty.scheme * context * state, Error.t) result

val infer_toplevel_let :
  state ->
  context ->
  Pattern.t ->
  Computation.t ->
  ((Variable.t * Ty.scheme) list * context * state, Error.t) result

val infer_toplevel_letrec :
  state ->
  context ->
  (Variable.t * Pattern.t * Restriction.t * Computation.t) list ->
  (Ty.scheme list * context * state, Error.t) result
