open Symbol

type t =
  | Basic of Const.Ty.t
  | Apply of t list * TyName.t
  | Arrow of Restriction.t * t * t
  | Param of Parameter.t
  | Tuple of t list
  | Handler of t * t

type scheme = Scheme of Parameter.t list * t

let int = Basic Const.Ty.Integer

let float = Basic Const.Ty.Float

let bool = Basic Const.Ty.Boolean

let string = Basic Const.Ty.String

let unit = Tuple []

let rec occurs_in p = function
  | Basic _ -> false
  | Apply (tys, _) -> List.exists (occurs_in p) tys
  | Param p' -> p = p'
  | Arrow (_, t1, t2) | Handler (t1, t2) -> occurs_in p t1 || occurs_in p t2
  | Tuple ts -> List.exists (occurs_in p) ts

let rec free_params ?(ps = []) = function
  | Basic _ -> ps
  | Apply (ts, _) -> List.fold_left (fun ps -> free_params ~ps) ps ts
  | Param p -> p :: ps
  | Arrow (_, t1, t2) | Handler (t1, t2) -> free_params ~ps:(free_params ~ps t1) t2
  | Tuple ts -> List.fold_left (fun ps -> free_params ~ps) ps ts

module Context = Map.Make (Parameter)

let rename_params bound t =
  let new_param = Parameter.init () in
  let rec rename ?(ctx = Context.empty) = function
    | Basic _ as t -> (t, ctx)
    | Param p -> (
        match Context.find_opt p ctx with
        | Some p' -> (Param p', ctx)
        | None ->
            let p' = new_param () in
            (Param p', Context.add p p' ctx) )
    | Apply (ts, name) ->
        let ts, ctx =
          ListLabels.fold_right ts ~init:([], ctx) ~f:(fun t (ts, ctx) ->
              let t, ctx = rename ~ctx t in
              (t :: ts, ctx))
        in
        (Apply (ts, name), ctx)
    | Arrow (restr, t1, t2) ->
        let t1, ctx = rename ~ctx t1 in
        let t2, ctx = rename ~ctx t2 in
        (Arrow (restr, t1, t2), ctx)
    | Handler (t1, t2) ->
        let t1, ctx = rename ~ctx t1 in
        let t2, ctx = rename ~ctx t2 in
        (Handler (t1, t2), ctx)
    | Tuple ts ->
        let ts, ctx =
          ListLabels.fold_right ts ~init:([], ctx) ~f:(fun t (ts, ctx) ->
              let t, ctx = rename ~ctx t in
              (t :: ts, ctx))
        in
        (Tuple ts, ctx)
  in
  let t, ctx = rename t in
  let bound = List.map (fun p -> Context.find_opt p ctx |> Option.value ~default:p) bound in
  (bound, t)

let print_ty ?(ps = []) =
  let rec pp prec t ppf =
    let print threshold fmt =
      if prec > threshold then Format.fprintf ppf ("(" ^^ fmt ^^ ")") else Format.fprintf ppf fmt
    in
    match t with
    | Basic cst -> Const.Ty.print cst ppf
    | Param p -> Parameter.print_weak ~is_weak:(not @@ List.mem p ps) p ppf
    | Apply ([], tyname) -> TyName.print tyname ppf
    | Apply ([ t ], tyname) -> print 3 "%t %t" (pp 3 t) (TyName.print tyname)
    | Apply (ts, tyname) -> print 3 "(%t) %t" (pp_list ~sep:", " 3 ts) (TyName.print tyname)
    | Tuple [] -> Format.fprintf ppf "unit"
    | Tuple ts -> print 2 "%t" (pp_list ~sep:" * " 2 ts)
    | Handler (t1, t2) -> print 1 "%t => %t" (pp 2 t1) (pp 1 t2)
    | Arrow (`Unrestricted, t1, t2) -> print 0 "%t -> %t" (pp 1 t1) (pp 0 t2)
    | Arrow (`AtMostOnce, t1, t2) -> print 0 "%t #-> %t" (pp 1 t1) (pp 0 t2)
  and pp_list ~sep prec ts ppf =
    match ts with
    | [] -> ()
    | [ t ] -> pp prec t ppf
    | t :: ts -> Format.fprintf ppf "%t%s%t" (pp prec t) sep (pp_list ~sep prec ts)
  in
  pp 0

let print (Scheme (ps, t)) ppf =
  let ps, t = rename_params ps t in
  print_ty ~ps t ppf
