type t =
  | Integer of int
  | Float of float
  | Boolean of bool
  | String of string

let print t ppf =
  match t with
  | Integer n -> Format.fprintf ppf "%d" n
  | Float f -> Format.fprintf ppf "%F" f
  | Boolean b -> Format.fprintf ppf "%B" b
  | String s -> Format.fprintf ppf "%S" s

module Ty = struct
  type t =
    | Integer
    | Float
    | Boolean
    | String

  let print t ppf =
    match t with
    | Integer -> Format.fprintf ppf "int"
    | Float -> Format.fprintf ppf "float"
    | Boolean -> Format.fprintf ppf "bool"
    | String -> Format.fprintf ppf "string"
end

let infer_ty = function
  | Integer _ -> Ty.Integer
  | Float _ -> Ty.Float
  | Boolean _ -> Ty.Boolean
  | String _ -> Ty.String
