module Ty : sig
  type t =
    | Integer
    | Float
    | Boolean
    | String

  val print : t -> Format.formatter -> unit
end

type t =
  | Integer of int
  | Float of float
  | Boolean of bool
  | String of string

val print : t -> Format.formatter -> unit

val infer_ty : t -> Ty.t
