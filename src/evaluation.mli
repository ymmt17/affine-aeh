open Symbol
open Output
open UntypedSyntax

type state

val new_state : state

val run : state -> Computation.t -> (Value.t, Error.t) result

val eval_toplevel_let :
  state ->
  (Variable.t * Ty.scheme) list ->
  Pattern.t ->
  Computation.t ->
  ((Variable.t * Ty.scheme * Value.t) list * state, Error.t) result

val eval_toplevel_letrec :
  state ->
  (Ty.scheme * Variable.t * Pattern.t * Computation.t) list ->
  ((Variable.t * Ty.scheme * Value.t) list * state, Error.t) result
