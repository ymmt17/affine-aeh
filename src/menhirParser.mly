%token LPAREN "("
%token RPAREN ")"
%token LBRACK "["
%token RBRACK "]"
%token UNDERSCORE "_"
%token HASH "#"
%token COMMA ","
%token SEMI ";"
%token SEMISEMI ";;"
%token COLON ":"
%token COLONCOLON "::"
%token BAR "|"
%token ARROW "->"
%token ARROW1 "#->"
%token DARROW "=>"
%token EQUAL "="
%token STAR "*"
%token EOF

%token AND
%token EFFECT
%token ELSE
%token FUN
%token FUNCTION
%token HANDLE
%token HANDLER
%token IF
%token IN
%token LET
%token LET1
%token MATCH
%token MATCH1
%token OF
%token PERFORM
%token REC
%token THEN
%token TYPE
%token WITH

%token <int> INT
%token <string> STRING
%token <float> FLOAT
%token <bool> BOOL
%token <string> LOWERCASE_IDENT
%token <string> UPPERCASE_IDENT
%token <string> TYPARAM

%token <string> INFIXOP_MUL
%token <string> INFIXOP_ADD
%token <string> INFIXOP_APPEND
%token <string> INFIXOP_CMP

%right SEMI
%left INFIXOP_CMP EQUAL
%right COLONCOLON
%left INFIXOP_APPEND
%left INFIXOP_ADD
%left INFIXOP_MUL STAR

%start <Declaration.t list> decls

%{
  open SugaredSyntax

  let collect_handler_clauses cls =
    let eff_cls, val_cl_opt =
      ListLabels.fold_left cls ~init:([], None) ~f:(fun (eff_cls, val_cl_opt) ->
        function
        | `EffClause (eff, p, k, t) ->
            ((eff, p, k, t) :: eff_cls, val_cl_opt)
        | `ValClause (p, t) ->
            (eff_cls, Option.fold val_cl_opt ~none:(Some (p, t)) ~some:Option.some))
    in
    let val_cl = Option.value ~default:(Pattern.Var "$id", Term.Var "$id") val_cl_opt in
    (eff_cls, val_cl)

  let nil_term = Term.Variant ("$nil", None)

  let cons_term t1 t2 = Term.Variant ("$cons", Some (Term.Tuple [t1; t2]))

  let nil_pat = Pattern.Variant ("$nil", None)

  let cons_pat p1 p2 = Pattern.Variant ("$cons", Some (Pattern.Tuple [p1; p2]))
%}

%%

let decls :=
  | decl = decl; SEMISEMI; decls = decls;
      { decl :: decls }
  | decl = decl; EOF;
      { [ decl ] }
  | EOF;
      { [] }

let decl :=
  | EFFECT; eff = UPPERCASE_IDENT; COLON; ty1 = product_ty; ARROW; ty2 = product_ty;
      { Declaration.Effect (eff, `Unrestricted, ty1, ty2) }
  | EFFECT; eff = UPPERCASE_IDENT; COLON; ty = product_ty;
      { Declaration.Effect (eff, `Unrestricted, Ty.Apply ([], "unit"), ty) }
  | TYPE; ps = params; HASH; tyname = LOWERCASE_IDENT; EQUAL; tydef = tydef;
      { Declaration.TyDef (`AtMostOnce, tyname, ps, tydef) }
  | TYPE; ps = params; tyname = LOWERCASE_IDENT; EQUAL; tydef = tydef;
      { Declaration.TyDef (`Unrestricted, tyname, ps, tydef) }
  | ~ = term;
      < Declaration.Term >
  | LET; REC; ~ = separated_list(AND, toplevel_letrec_binding);
      < Declaration.LetRec >
  | LET; (p, t) = let_binding;
      { Declaration.Let (p, t) }

let tydef :=
  | ~ = ty;
      < TyDef.Alias >
  | BAR?; ~ = separated_list(BAR, constructor);
      < TyDef.Variant >

let constructor :=
  | label = UPPERCASE_IDENT; OF; ty = ty;
      { (label, Some ty) }
  | label = UPPERCASE_IDENT;
      { (label, None) }

let toplevel_letrec_binding :=
  | ~ = ident; ~ = let_binding0;
      <>

let term :=
  | FUN; p = simple_pattern; ps = simple_pattern*; ARROW; t = term;
      { let t = List.fold_right (fun p t -> Term.Lambda (`Unrestricted, p, t)) ps t in
        Term.Lambda (`Unrestricted, p, t)
      }
  | FUN; p = simple_pattern; ps = simple_pattern*; ARROW1; t = term;
      { let t = List.fold_right (fun p t -> Term.Lambda (`AtMostOnce, p, t)) ps t in
        Term.Lambda (`AtMostOnce, p, t)
      }
  | FUNCTION; ~ = match_cases;
      < Term.Function >
  | LET; REC; ~ = ident; ~ = let_binding0; IN; ~ = term;
      < Term.LetRec >
  | LET; (p, t1) = let_binding; IN; t2 = term;
      { Term.Let (`Unrestricted, p, t1, t2) }
  | LET1; (p, t1) = let_binding; IN; t2 = term;
      { Term.Let (`AtMostOnce, p, t1, t2) }
  | WITH; t1 = term; HANDLE; t2 = term;
      { Term.Handle (t1, t2) }
  | HANDLE; t1 = term; WITH; t2 = handler;
      { Term.Handle (t2, t1) }
  | IF; cond = term; THEN; thn = term; ELSE; els = term;
      { Term.If (cond, thn, els) }
  | MATCH; cond = term; WITH; cases = match_cases;
      { Term.Match (`Unrestricted, cond, cases) }
  | MATCH1; cond = term; WITH; cases = match_cases;
      { Term.Match (`AtMostOnce, cond, cases) }
  | t1 = term; SEMI; t2 = term;
      { Term.Let (`AtMostOnce, Pattern.Discard, t1, t2) }
  | ~ = tuple_term;
      <>

let tuple_term :=
  | ts = separated_nonempty_list(COMMA, cons_term);
      { match ts with
        | [ t ] -> t
        | ts -> Term.Tuple ts
      }

let cons_term :=
  | t1 = binop_term; COLONCOLON; t2 = cons_term;
      { cons_term t1 t2 }
  | ~ = binop_term;
      <>

let binop_term :=
  | t1 = binop_term; op = infixop; t2 = binop_term;
      { let restr = if op.[String.length op - 1] = '#' then `AtMostOnce else `Unrestricted in
        Term.(Apply (restr, Apply (restr, Var op, t1), t2))
      }
  | ~ = app_term;
      <>

let app_term :=
  | label = UPPERCASE_IDENT; t = simple_term;
      { Term.Variant (label, Some t) }
  | t1 = app_term; HASH; t2 = simple_term;
      { Term.Apply (`AtMostOnce, t1, t2) }
  | t1 = app_term; t2 = simple_term;
      { Term.Apply (`Unrestricted, t1, t2) }
  | ~ = simple_term;
      <>

let simple_term :=
  | ~ = const;
      < Term.Const >
  | ~ = ident;
      < Term.Var >
  | label = UPPERCASE_IDENT;
      { Term.Variant (label, None) }
  | LPAREN; RPAREN;
      { Term.Tuple [] }
  | LPAREN; ~ = term; COLON; ~ = ty; RPAREN;
      < Term.Annotated >
  | LPAREN; ~ = term; RPAREN;
      <>
  | LBRACK; ts = separated_list(SEMI, simple_term); SEMI?; RBRACK;
      { ListLabels.fold_right ts ~init:nil_term ~f:cons_term }
  | PERFORM; LPAREN; ~ = UPPERCASE_IDENT; ~ = term; RPAREN;
      < Term.Effect >
  | PERFORM; eff = UPPERCASE_IDENT;
      { Term.Effect (eff, Term.Tuple []) }
  | HANDLER; ~ = handler;
      <>

let let_binding0 :=
  | EQUAL; ~ = term;
      <>
  | ps = pattern*; EQUAL; t = term;
      { List.fold_right (fun p t -> Term.Lambda (`Unrestricted, p, t)) ps t }

let let_binding :=
  | ~ = pattern; EQUAL; ~ = term;
      <>
  | x = ident; ps = pattern+; EQUAL; t = term;
      { let t = List.fold_right (fun p t -> Term.Lambda (`Unrestricted, p, t)) ps t in
        (Pattern.Var x, t)
      }

let match_case :=
  | ~ = pattern; ARROW; ~ = term;
      <>

let match_cases :=
  | BAR?; ~ = separated_list(BAR, match_case);
      <>

let handler :=
  | BAR?; cls = separated_nonempty_list(BAR, handler_clause);
      { let eff_clauses, val_clause = collect_handler_clauses cls in
        Term.Handler { eff_clauses; val_clause }
      }

let handler_clause :=
  | EFFECT; LPAREN; eff = UPPERCASE_IDENT; p = simple_pattern; RPAREN; k = simple_pattern; ARROW; t = term;
      { `EffClause (eff, p, k, t) }
  | EFFECT; eff = UPPERCASE_IDENT; k = simple_pattern; ARROW; t = term;
      { `EffClause (eff, Pattern.Discard, k, t) }
  | ~ = pattern; ARROW; ~ = term;
      < `ValClause >

let pattern :=
  | ps = separated_nonempty_list(COMMA, variant_pattern);
      { match ps with
        | [ p ] -> p
        | ps -> Pattern.Tuple ps
      }

let variant_pattern :=
  | label = UPPERCASE_IDENT; p = cons_pattern;
      { Pattern.Variant (label, Some p) }
  | label = UPPERCASE_IDENT;
      { Pattern.Variant (label, None) }
  | ~ = cons_pattern;
      <>

let cons_pattern :=
  | p1 = simple_pattern; COLONCOLON; p2 = cons_pattern;
      { cons_pat p1 p2 }
  | ~ = simple_pattern;
      <>

let simple_pattern :=
  | ~ = const;
      < Pattern.Const >
  | UNDERSCORE;
      { Pattern.Discard }
  | ~ = ident;
      < Pattern.Var >
  | label = UPPERCASE_IDENT;
      { Pattern.Variant (label, None) }
  | LBRACK; ps = separated_list(SEMI, simple_pattern); SEMI?; RBRACK;
      { ListLabels.fold_right ps ~init:nil_pat ~f:cons_pat }
  | LPAREN; RPAREN;
      { Pattern.Tuple [] }
  | LPAREN; ~ = pattern; RPAREN;
      <>

let ty :=
  | ty1 = handler_ty; ARROW; ty2 = ty;
      { Ty.Arrow (`Unrestricted, ty1, ty2) }
  | ty1 = handler_ty; ARROW1; ty2 = ty;
      { Ty.Arrow (`AtMostOnce, ty1, ty2) }
  | ~ = handler_ty;
      <>

let handler_ty :=
  | ty1 = product_ty; DARROW; ty2 = handler_ty;
      { Ty.Handler (ty1, ty2) }
  | ~ = product_ty;
      <>

let product_ty :=
  | tys = separated_nonempty_list(STAR, apply_ty);
      { match tys with
        | [ ty ] -> ty
        | tys -> Ty.Tuple tys
      }

let apply_ty :=
  | LPAREN; ~ = separated_nonempty_list(COMMA, ty); RPAREN; ~ = LOWERCASE_IDENT;
      < Ty.Apply >
  | ty = apply_ty; tyname = LOWERCASE_IDENT;
      { Ty.Apply ([ ty ], tyname) }
  | ~ = simple_ty;
      <>

let simple_ty :=
  | tyname = LOWERCASE_IDENT;
      { Ty.Apply ([], tyname) }
  | ~ = TYPARAM;
      < Ty.Param >
  | LPAREN; ~ = ty; RPAREN;
      <>

let const :=
  | ~ = INT;
      < Const.Integer >
  | ~ = FLOAT;
      < Const.Float >
  | ~ = BOOL;
      < Const.Boolean >
  | ~ = STRING;
      < Const.String >

let ident :=
  | ~ = LOWERCASE_IDENT;
      <>
  | LPAREN; ~ = infixop; RPAREN;
      <>

let infixop ==
  | EQUAL;
      { "=" }
  | ~ = INFIXOP_CMP;
      <>
  | ~ = INFIXOP_APPEND;
      <>
  | ~ = INFIXOP_ADD;
      <>
  | STAR;
      { "*" }
  | ~ = INFIXOP_MUL;
      <>

let params :=
  | LPAREN; ~ = separated_nonempty_list(COMMA, TYPARAM); RPAREN;
      <>
  | p = TYPARAM;
      { [ p ] }
  |
      { [] }
