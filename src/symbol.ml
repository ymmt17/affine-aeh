module type S = sig
  type annot

  type t = {
    id : int;
    annot : annot;
  }

  val compare : t -> t -> int

  val init : unit -> annot -> t

  val make : annot -> t

  val print : t -> Format.formatter -> unit
end

module Make (Annot : sig
  type t
end) =
struct
  type t = {
    id : int;
    annot : Annot.t;
  }

  let compare x y = compare x.id y.id

  let init () =
    let counter = ref (-1) in
    fun annot ->
      incr counter;
      { id = !counter; annot }

  let make = init ()
end

module Variable = struct
  include Make (struct
    type t = string
  end)

  let print t ppf = Format.fprintf ppf "%s" t.annot
end

module Effect = struct
  include Make (struct
    type t = string
  end)

  let print t ppf = Format.fprintf ppf "%s" t.annot
end

module TyName = struct
  include Make (struct
    type t = string
  end)

  let print t ppf = Format.fprintf ppf "%s" t.annot
end

module Label = struct
  include Make (struct
    type t = string
  end)

  let print t ppf = Format.fprintf ppf "%s" t.annot
end

module Parameter = struct
  include Make (struct
    type t = unit
  end)

  let print_weak ~is_weak t ppf =
    let c = Char.chr ((t.id mod 26) + 97) and d = t.id / 26 in
    let prefix = if is_weak then "'_" else "'" in
    if d = 0 then Format.fprintf ppf "%s%c" prefix c else Format.fprintf ppf "%s%c%d" prefix c d

  let print = print_weak ~is_weak:false
end

module Context = Map.Make (String)
module VarContext = Map.Make (Variable)
module EffContext = Map.Make (Effect)
module TyNameContext = Map.Make (TyName)
module LabelContext = Map.Make (Label)
