open Symbol

module TyDef = struct
  type t =
    | Alias of Ty.t
    | Variant of (Restriction.t * Ty.t option) LabelContext.t
end

module Pattern = struct
  type t =
    | Const of Const.t
    | Var of Variable.t
    | Variant of Label.t * t option
    | Tuple of t list
    | Discard
end

module rec Expression : sig
  type t =
    | Annotated of t * Ty.t
    | Const of Const.t
    | Lambda of Restriction.t * Pattern.t * Computation.t
    | Tuple of t list
    | Var of Variable.t
    | Variant of Label.t * t option
    | Handler of handler

  and handler = {
    eff_clauses : (Pattern.t * Pattern.t * Computation.t) EffContext.t;
    val_clause : Pattern.t * Computation.t;
  }
end = struct
  type t =
    | Annotated of t * Ty.t
    | Const of Const.t
    | Lambda of Restriction.t * Pattern.t * Computation.t
    | Tuple of t list
    | Var of Variable.t
    | Variant of Label.t * t option
    | Handler of handler

  and handler = {
    eff_clauses : (Pattern.t * Pattern.t * Computation.t) EffContext.t;
    val_clause : Pattern.t * Computation.t;
  }
end

and Computation : sig
  type t =
    | Apply of Restriction.t * Expression.t * Expression.t
    | Effect of Effect.t * Expression.t
    | Let of Restriction.t * Pattern.t * t * t
    | LetRec of Variable.t * Pattern.t * Restriction.t * t * t
    | Value of Expression.t
    | Handle of Expression.t * t
    | If of Expression.t * t * t
    | Match of Restriction.t * Expression.t * (Pattern.t * t) list
end = struct
  type t =
    | Apply of Restriction.t * Expression.t * Expression.t
    | Effect of Effect.t * Expression.t
    | Let of Restriction.t * Pattern.t * t * t
    | LetRec of Variable.t * Pattern.t * Restriction.t * t * t
    | Value of Expression.t
    | Handle of Expression.t * t
    | If of Expression.t * t * t
    | Match of Restriction.t * Expression.t * (Pattern.t * t) list
end
