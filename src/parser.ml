open Angstrom
open SugaredSyntax
module Reserved = Set.Make (String)

let keywords =
  List.fold_left (Fun.flip Reserved.add) Reserved.empty
    [
      "and";
      "false";
      "true";
      "fun";
      "function";
      "perform";
      "effect";
      "let";
      "rec";
      "in";
      "handler";
      "handle";
      "with";
      "match";
      "if";
      "then";
      "else";
    ]

let skip_while1 pred = skip pred *> skip_while pred

let in_comment comment =
  fix @@ fun in_comment ->
  choice
    [
      char '*' *> (skip (( = ) ')') <|> in_comment);
      char '(' *> (comment <|> in_comment);
      skip_while1 (fun c -> c <> '(' && c <> '*') *> in_comment;
    ]

let comment = char '(' *> fix (fun comment -> char '*' *> in_comment comment)

let whitespace =
  skip_while1 (function
    | ' ' | '\t' | '\r' | '\n' -> true
    | _ -> false)

let space = skip_many (whitespace <|> comment)

let int =
  let* sign = option ( ~+ ) (char '-' *> return ( ~- )) in
  take_while1 (function
    | '0' .. '9' -> true
    | _ -> false)
  >>| int_of_string >>| sign <?> "integer"

let float =
  let* sign = option Fun.id (char '-' *> return Float.neg) in
  consumed
    ( take_while1 (function
        | '0' .. '9' -> true
        | _ -> false)
    *> char '.'
    *> take_while (function
         | '0' .. '9' -> true
         | _ -> false)
    *> option ""
         ( string_ci "e"
         *> option "" (string "+" <|> string "-")
         *> take_while1 (function
              | '0' .. '9' -> true
              | _ -> false) ) )
  >>| float_of_string >>| sign <?> "floating number"

let bool = string "true" <|> string "false" >>| bool_of_string <?> "boolean"

let lowercase_ident =
  let* lit =
    consumed
      ( satisfy (function
          | 'a' .. 'z' | '_' -> true
          | _ -> false)
      *> take_while (function
           | 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' | '\'' -> true
           | _ -> false) )
  in
  if Reserved.mem lit keywords then fail "reserved keyword" else return lit

let uppercase_ident =
  consumed
    ( satisfy (function
        | 'A' .. 'Z' -> true
        | _ -> false)
    *> take_while (function
         | 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' | '\'' -> true
         | _ -> false) )

let operator_char = choice (List.map char [ '#'; '.'; '='; '<'; '>' ])

let infixop_mul = consumed (choice [ char '*'; char '/' ] *> many operator_char)

let infixop_add = consumed (choice [ char '+'; char '-' ] *> many operator_char)

let infixop_compose = consumed (choice [ char '^'; char '@' ] *> many operator_char)

let infixop_cmp = consumed (choice [ char '='; char '<'; char '>' ] *> many operator_char)

let infixop = choice [ infixop_mul; infixop_add; infixop_compose; infixop_cmp ]

let ident =
  choice
    [
      lowercase_ident;
      (let* _ = char '(' *> space in
       let* op = infixop in
       let* _ = char ')' *> space in
       return op);
    ]

let param = char '\'' *> lowercase_ident <* space

let simple_ty ty =
  choice
    [
      (let* name = lowercase_ident <* space in
       return (Ty.Apply ([], name)));
      (let* p = param in
       return (Ty.Param p));
      (let* _ = char '(' <* space in
       let* ty = ty in
       let* _ = char ')' <* space in
       return ty);
    ]

let apply_ty ty =
  choice
    [
      (let* _ = char '(' <* space in
       let* tys = sep_by1 (char ',' <* space) ty in
       let* _ = char ')' <* space in
       let* name = lowercase_ident <* space in
       return (Ty.Apply (tys, name)));
      (let* ty = simple_ty ty in
       let* names = many1 (lowercase_ident <* space) in
       return (List.fold_left (fun ty name -> Ty.Apply ([ ty ], name)) ty names));
      simple_ty ty;
    ]

let product_ty ty =
  sep_by1 (char '*' <* space) (apply_ty ty) >>| function
  | [ ty ] -> ty
  | tys -> Ty.Tuple tys

let ty =
  fix @@ fun ty ->
  choice
    [
      (let* ty1 = product_ty ty in
       let* restr = option `Unrestricted (char '#' *> return `AtMostOnce) in
       let* _ = string "->" <* space in
       let* ty2 = ty in
       return (Ty.Arrow (restr, ty1, ty2)));
      product_ty ty;
    ]

let const =
  choice
    [
      (let* f = float <* space in
       return (Const.Float f));
      (let* n = int <* space in
       return (Const.Integer n));
      (let* b = bool <* space in
       return (Const.Boolean b));
      (let* _ = char '"' in
       let* s =
         take_while (function
           | '"' -> false
           | _ -> true)
       in
       let* _ = char '"' <* space in
       return (Const.String s));
    ]

let rec build_cons_pattern0 = function
  | [] -> Pattern.Variant ("$nil", None)
  | p :: ps -> Pattern.Variant ("$cons", Some (Pattern.Tuple [ p; build_cons_pattern0 ps ]))

let simple_pattern pattern =
  fix @@ fun simple_pattern ->
  choice
    [
      (let* cst = const in
       return (Pattern.Const cst));
      (let* _ = char '_' <* space in
       return Pattern.Discard);
      (let* x = ident <* space in
       return (Pattern.Var x));
      (let* label = uppercase_ident <* space in
       return (Pattern.Variant (label, None)));
      (let* _ = char '[' <* space in
       let* ps = sep_by (char ';' <* space) simple_pattern in
       let* _ = option "" (string ";") <* space in
       let* _ = char ']' <* space in
       return (build_cons_pattern0 ps));
      (let* _ = char '(' <* space in
       let* _ = char ')' <* space in
       return (Pattern.Tuple []));
      (let* _ = char '(' <* space in
       let* p = pattern in
       let* _ = char ')' <* space in
       return p);
    ]

let rec build_cons_pattern = function
  | [ p ] -> p
  | p :: ps -> Pattern.Variant ("$cons", Some (Pattern.Tuple [ p; build_cons_pattern ps ]))
  | _ -> assert false

let cons_pattern pattern =
  let* p = simple_pattern pattern in
  many
    (let* _ = string "::" <* space in
     simple_pattern pattern)
  >>| function
  | [] -> p
  | ps -> build_cons_pattern (p :: ps)

let variant_pattern pattern =
  choice
    [
      (let* label = uppercase_ident <* space in
       let* popt = option None (cons_pattern pattern >>| Option.some) in
       return (Pattern.Variant (label, popt)));
      cons_pattern pattern;
    ]

let pattern =
  fix (fun pattern ->
      sep_by1 (char ',' *> space) (variant_pattern pattern) >>| function
      | [ p ] -> p
      | ps -> Pattern.Tuple ps)

let handler_clause term =
  choice
    [
      (let* _ = string "effect" <* space in
       let* eff, v =
         choice
           [
             (let* _ = char '(' <* space in
              let* eff = uppercase_ident <* space in
              let* p = simple_pattern pattern <* space in
              let* _ = char ')' <* space in
              return (eff, p));
             (let* eff = uppercase_ident <* space in
              return (eff, Pattern.Discard));
           ]
       in
       let* k = simple_pattern pattern <* space in
       let* _ = string "->" <* space in
       let* t = term in
       return (`EffClause (eff, v, k, t)));
      (let* p = pattern <* space in
       let* _ = string "->" <* space in
       let* t = term in
       return (`ValClause (p, t)));
    ]

let collect_handler_clauses cls =
  let eff_cls, val_cl_opt =
    ListLabels.fold_left cls ~init:([], None) ~f:(fun (eff_cls, val_cl_opt) ->
      function
      | `EffClause (eff, p, k, t) ->
          let eff_cls = (eff, p, k, t) :: eff_cls in
          (eff_cls, val_cl_opt)
      | `ValClause (p, t) ->
          let val_cl_opt = Option.fold ~none:(Some (p, t)) ~some:Option.some val_cl_opt in
          (eff_cls, val_cl_opt))
  in
  let val_cl = Option.value ~default:(Pattern.Var "$id", Term.Var "$id") val_cl_opt in
  (eff_cls, val_cl)

let handler term =
  let* _ = option "" (string "|") <* space in
  let* cls = sep_by1 (char '|' <* space) (handler_clause term) in
  let eff_clauses, val_clause = collect_handler_clauses cls in
  return (Term.Handler { eff_clauses; val_clause })

let rec build_cons_term0 = function
  | [] -> Term.Variant ("$nil", None)
  | t :: ts -> Term.Variant ("$cons", Some (Term.Tuple [ t; build_cons_term0 ts ]))

let rec build_cons_term = function
  | [ t ] -> t
  | t :: ts -> Term.Variant ("$cons", Some (Term.Tuple [ t; build_cons_term ts ]))
  | _ -> assert false

let rec build_cons1_term0 = function
  | [] -> Term.Variant ("$nil1", None)
  | t :: ts -> Term.Variant ("$cons1", Some (Term.Tuple [ t; build_cons1_term0 ts ]))

let simple_term term =
  fix @@ fun simple_term ->
  choice
    [
      (let* cst = const in
       return (Term.Const cst));
      (let* x = ident <* space in
       return (Term.Var x));
      (let* label = uppercase_ident <* space in
       return (Term.Variant (label, None)));
      (let* _ = string "perform" <* space in
       choice
         [
           (let* _ = char '(' <* space in
            let* eff = uppercase_ident <* space in
            let* t = term in
            let* _ = char ')' <* space in
            return (Term.Effect (eff, t)));
           (let* eff = uppercase_ident <* space in
            return (Term.Effect (eff, Term.Tuple [])));
         ]);
      (let* _ = string "handler" <* space in
       handler term);
      (let* _ = char '[' <* space in
       let* ts = sep_by (char ';' <* space) simple_term in
       let* _ = option "" (string ";") <* space in
       let* _ = string "]#" <* space in
       return (build_cons1_term0 ts));
      (let* _ = char '[' <* space in
       let* ts = sep_by (char ';' <* space) simple_term in
       let* _ = option "" (string ";") <* space in
       let* _ = char ']' <* space in
       return (build_cons_term0 ts));
      (let* _ = char '(' <* space in
       let* _ = char ')' <* space in
       return (Term.Tuple []));
      (let* _ = char '(' <* space in
       let* t = term in
       let* _ = char ')' <* space in
       return t);
      (let* _ = char '(' <* space in
       let* t = term in
       let* _ = char ':' <* space in
       let* ty = ty in
       let* _ = char ')' <* space in
       return (Term.Annotated (t, ty)));
    ]

(* let app_term term =
  choice
    [
      (let* label = uppercase_ident <* space in
       let* t = simple_term term in
       return (Term.Variant (label, Some t)));
      (let* t = simple_term term in
       let* _ = string "#" <* space in
       let* ts = sep_by (string "#" *> space) (simple_term term) in
       return (List.fold_left (fun t1 t2 -> Term.Apply1 (t1, t2)) t ts));
      (let* t = simple_term term in
       let* ts = many (simple_term term) in
       return (List.fold_left (fun t1 t2 -> Term.Apply (t1, t2)) t ts));
    ] *)

let app_term term =
  choice
    [
      (let* label = uppercase_ident <* space in
       let* t = simple_term term in
       return (Term.Variant (label, Some t)));
      (let* t = simple_term term in
       let* ts =
         many
           (let* restr = option `Unrestricted (char '#' *> return `AtMostOnce) <* space in
            let* t = simple_term term in
            return (restr, t))
       in
       return (List.fold_left (fun t1 (restr, t2) -> Term.Apply (restr, t1, t2)) t ts));
    ]

let binop_term term op next =
  let* t = next term in
  many
    (let* op = op <* space in
     let* t = next term in
     return (op, t))
  >>| ListLabels.fold_left ~init:t ~f:(fun t1 (op, t2) ->
          let restr = if op.[String.length op - 1] = '#' then `AtMostOnce else `Unrestricted in
          Term.(Apply (restr, Apply (restr, Var op, t1), t2)))

(* let binop_term term op next =
  let* t = next term in
  many
    (let* op = op <* space in
     let* t = next term in
     return (op, t))
  >>| List.fold_left (fun t1 (op, t2) -> Term.(Apply (Apply (Term.Var op, t1), t2))) t *)

let mul_term term = binop_term term infixop_mul app_term

let add_term term = binop_term term infixop_add mul_term

let compose_term term = binop_term term infixop_compose add_term

let cmp_term term = binop_term term infixop_cmp compose_term

let cons_term term =
  let* t = cmp_term term in
  many
    (let* _ = string "::" <* space in
     cmp_term term)
  >>| function
  | [] -> t
  | ts -> build_cons_term (t :: ts)

let tuple_term term =
  sep_by1 (char ',' *> space) (cons_term term) >>| function
  | [ t ] -> t
  | ts -> Term.Tuple ts

let match_cases term =
  let* _ = option () (char '|' *> space) in
  sep_by
    (char '|' *> space)
    (let* p = pattern in
     let* _ = string "->" <* space in
     let* t = term in
     return (p, t))

let let_binding0 term =
  choice
    [
      (let* _ = char '=' <* space in
       let* t = term in
       return t);
      (let* args = many pattern in
       let* _ = char '=' <* space in
       let* t = term in
       let t = List.fold_right (fun p t -> Term.Lambda (`Unrestricted, p, t)) args t in
       return t);
    ]

let let_binding term =
  choice
    [
      (let* p = pattern in
       let* _ = char '=' <* space in
       let* t = term in
       return (p, t));
      (let* x = ident <* space in
       let* args = many1 pattern in
       let* _ = char '=' <* space in
       let* t = term in
       let t = List.fold_right (fun p t -> Term.Lambda (`Unrestricted, p, t)) args t in
       return (Pattern.Var x, t));
    ]

let term =
  fix @@ fun term ->
  choice
    [
      (let* _ = string "fun" <* space in
       let* p = simple_pattern pattern in
       let* ps = many (simple_pattern pattern) in
       let* restr = option `Unrestricted (char '#' *> return `AtMostOnce) in
       let* _ = string "->" <* space in
       let* t = term in
       let t = List.fold_right (fun p t -> Term.Lambda (restr, p, t)) ps t in
       return (Term.Lambda (restr, p, t)));
      (let* _ = string "let" <* space in
       let* _ = string "rec" <* space in
       let* x = ident <* space in
       let* t1 = let_binding0 term in
       let* _ = string "in" <* space in
       let* t2 = term in
       return (Term.LetRec (x, t1, t2)));
      (let* _ = string "let" in
       let* restr = option `Unrestricted (char '#' *> return `AtMostOnce) <* space in
       let* p, t1 = let_binding term in
       let* _ = string "in" <* space in
       let* t2 = term in
       return (Term.Let (restr, p, t1, t2)));
      (let* _ = string "with" <* space in
       let* t1 = term in
       let* _ = string "handle" <* space in
       let* t2 = term in
       return (Term.Handle (t1, t2)));
      (let* _ = string "handle" <* space in
       let* t1 = term in
       let* _ = string "with" <* space in
       let* t2 = handler term in
       return (Term.Handle (t2, t1)));
      (let* _ = string "if" <* space in
       let* cond = term in
       let* _ = string "then" <* space in
       let* thn = term in
       let* _ = string "else" <* space in
       let* els = term in
       return (Term.If (cond, thn, els)));
      (let* _ = string "match" in
       let* restr = option `Unrestricted (char '#' *> return `AtMostOnce) <* space in
       let* cond = term in
       let* _ = string "with" <* space in
       let* cases = match_cases term in
       return (Term.Match (restr, cond, cases)));
      (let* _ = string "function" <* space in
       let* cases = match_cases term in
       return (Term.Function cases));
      (let* t1 = tuple_term term in
       option t1
         (let* _ = char ';' <* space in
          let* t2 = term in
          return (Term.Let (`AtMostOnce, Pattern.Discard, t1, t2))));
    ]

let params =
  choice
    [
      (let* _ = char '(' <* space in
       let* ps = sep_by1 (char ',' <* space) param in
       let* _ = char ')' <* space in
       return ps);
      option [] (param >>| fun p -> [ p ]);
    ]

let type_definition =
  choice
    [
      (let* ty = ty in
       return (TyDef.Alias ty));
      (let* _ = option "" (string "|") <* space in
       let* constrs =
         sep_by
           (char '|' <* space)
           (let* label = uppercase_ident <* space in
            let* tyopt =
              option None
                (let* _ = string "of" <* space in
                 Option.some <$> ty)
            in
            return (label, tyopt))
       in
       return (TyDef.Variant constrs));
    ]

let definition =
  choice
    [
      (let* _ = string "effect" <* space in
       let* eff = uppercase_ident <* space in
       let* _ = char ':' <* space in
       let* restr, ty1, ty2 =
         choice
           [
             (let* ty1 = product_ty ty in
              let* restr = option `Unrestricted (char '#' *> return `AtMostOnce) in
              let* _ = string "->" <* space in
              let* ty2 = product_ty ty in
              return (restr, ty1, ty2));
             (let* ty = product_ty ty in
              return (`AtMostOnce, Ty.Apply ([], "unit"), ty));
           ]
       in
       return (Declaration.Effect (eff, restr, ty1, ty2)));
      (let* _ = string "type" <* space in
       let* params = params in
       let* restr = option `Unrestricted (char '#' *> return `AtMostOnce) in
       let* tyname = lowercase_ident <* space in
       let* _ = char '=' <* space in
       let* def = type_definition in
       return (Declaration.TyDef (restr, tyname, params, def)));
      (let* t = term in
       return (Declaration.Term t));
      (* (let* _ = string "let" <* space in
         let* _ = string "rec" <* space in
         let* x = ident <* space in
         let* t = let_binding0 term in
         return (Declaration.LetRec (x, t))); *)
      (let* _ = string "let" <* space in
       let* _ = string "rec" <* space in
       let* binds =
         sep_by
           (string "and" <* space)
           (let* x = ident <* space in
            let* t = let_binding0 term in
            return (x, t))
       in
       return (Declaration.LetRec binds));
      (let* _ = string "let" <* space in
       let* p, t = let_binding term in
       return (Declaration.Let (p, t)));
    ]

let definitions =
  let* _ = space in
  let* defs = sep_by1 (string ";;" <* space) definition in
  let* _ = option "" (string ";;") <* space in
  let* _ = end_of_input in
  return defs

let read_string src =
  parse_string ~consume:Consume.All definitions src
  |> Result.map_error (fun msg -> `ParseFailed msg)

let read_file path =
  let in_channel = open_in path in
  let buf = Bytes.create 256000 in
  let len = input in_channel buf 0 256000 in
  close_in in_channel;
  read_string @@ Bytes.sub_string buf 0 len

(* let read_string src =
  let state = Buffered.parse (space *> definitions) in
  let state = Buffered.feed state (`String src) in
  let state = Buffered.feed state `Eof in
  Buffered.state_to_result state |> Result.map_error (fun msg -> Error.ParseFailed msg) *)
