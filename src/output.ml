open Symbol

module rec Value : sig
  type t =
    | Const of Const.t
    | Closure of (t -> (Output.t, Error.t) result)
    | Variant of Label.t * t option
    | Tuple of t list
    | Handler of handler

  and handler = Output.t -> (Output.t, Error.t) result

  val print : t -> Format.formatter -> unit

  val to_int : t -> int

  val to_float : t -> float

  val to_bool : t -> bool

  val to_string : t -> string
end = struct
  type t =
    | Const of Const.t
    | Closure of (t -> (Output.t, Error.t) result)
    | Variant of Label.t * t option
    | Tuple of t list
    | Handler of handler

  and handler = Output.t -> (Output.t, Error.t) result

  let rec print_tuple ts ppf =
    match ts with
    | [] -> ()
    | [ t ] -> Format.fprintf ppf "%t" (print t)
    | t :: ts -> Format.fprintf ppf "%t, %t" (print t) (print_tuple ts)

  and print_list t ppf =
    match t with
    | Variant (label1, Some (Tuple [ t; Variant (label2, None) ]))
      when label1.annot = "$cons" && label2.annot = "$nil" ->
        Format.fprintf ppf "%t" (print t)
    | Variant (label, Some (Tuple [ t; ts ])) when label.annot = "$cons" ->
        Format.fprintf ppf "%t; %t" (print t) (print_list ts)
    | _ -> assert false

  and print t ppf =
    match t with
    | Const cst -> Const.print cst ppf
    | Closure _ -> Format.fprintf ppf "<fun>"
    | Handler _ -> Format.fprintf ppf "<handler>"
    | Variant (label, None) when label.annot = "$nil" -> Format.fprintf ppf "[]"
    | Variant (label, None) -> Label.print label ppf
    | Variant (label, Some _) as t when label.annot = "$cons" ->
        Format.fprintf ppf "[%t]" (print_list t)
    | Variant (label, Some t) -> Format.fprintf ppf "%t %t" (Label.print label) (print t)
    | Tuple [] -> Format.fprintf ppf "()"
    | Tuple [ t ] -> Format.fprintf ppf "%t" (print t)
    | Tuple ts -> Format.fprintf ppf "(%t)" (print_tuple ts)

  let to_int = function
    | Const (Const.Integer n) -> n
    | _ -> assert false

  let to_float = function
    | Const (Const.Float f) -> f
    | _ -> assert false

  let to_bool = function
    | Const (Const.Boolean b) -> b
    | _ -> assert false

  let to_string = function
    | Const (Const.String s) -> s
    | _ -> assert false
end

and Output : sig
  type t =
    | Val of Value.t
    | Eff of Effect.t * Value.t * (Value.t -> (t, Error.t) result)

  val print : t -> Format.formatter -> unit
end = struct
  type t =
    | Val of Value.t
    | Eff of Effect.t * Value.t * (Value.t -> (t, Error.t) result)

  let print t ppf =
    match t with
    | Val v -> Value.print v ppf
    | Eff _ -> Format.fprintf ppf "<effect>"
end
