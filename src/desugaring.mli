module Sugared = SugaredSyntax
open Symbol
open UntypedSyntax

type state

val new_state : state

val desugar_comp :
  state -> ?restr:Restriction.t -> Sugared.Term.t -> (Computation.t * state, Error.t) result

val desugar_toplevel_let :
  state ->
  Sugared.Pattern.t ->
  Sugared.Term.t ->
  (Pattern.t * Computation.t * state, Error.t) result

val desugar_toplevel_letrec :
  state ->
  (string * Sugared.Term.t) list ->
  ((Variable.t * Pattern.t * Restriction.t * Computation.t) list * state, Error.t) result

val desugar_effect :
  state ->
  string ->
  Restriction.t ->
  Sugared.Ty.t ->
  Sugared.Ty.t ->
  (Effect.t * Ty.t * Ty.t * state, Error.t) result

val desugar_newtype :
  state ->
  restr:Restriction.t ->
  string list ->
  string ->
  Sugared.TyDef.t ->
  (TyName.t * Parameter.t list * TyDef.t * state, Error.t) result
