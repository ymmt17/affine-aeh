open Symbol
open Output
open UntypedSyntax

val var_context : Variable.t Context.t

val var_ty_context : Ty.scheme VarContext.t

val var_instance_context : Value.t VarContext.t

val eff_context : (Effect.t * Restriction.t) Context.t

val eff_ty_context : (Restriction.t * Ty.t * Ty.t) EffContext.t

val tyname_context : TyName.t Context.t

val tydef_context : (Parameter.t list * TyDef.t) TyNameContext.t

val constr_context : (Label.t * Restriction.t * int) Context.t

val constr_ty_context : (TyName.t * Parameter.t list * Restriction.t * Ty.t option) LabelContext.t
