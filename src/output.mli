open Symbol

module rec Value : sig
  type t =
    | Const of Const.t
    | Closure of (t -> (Output.t, Error.t) result)
    | Variant of Label.t * t option
    | Tuple of t list
    | Handler of handler

  and handler = Output.t -> (Output.t, Error.t) result

  val print : t -> Format.formatter -> unit

  val to_int : t -> int

  val to_float : t -> float

  val to_bool : t -> bool

  val to_string : t -> string
end

and Output : sig
  type t =
    | Val of Value.t
    | Eff of Effect.t * Value.t * (Value.t -> (t, Error.t) result)

  val print : t -> Format.formatter -> unit
end
