module Ty = struct
  type t =
    | Tuple of t list
    | Apply of t list * string
    | Param of string
    | Arrow of Restriction.t * t * t
    | Handler of t * t
end

module TyDef = struct
  type t =
    | Alias of Ty.t
    | Variant of (string * Ty.t option) list
end

module Pattern = struct
  type t =
    | Const of Const.t
    | Var of string
    | Variant of string * t option
    | Tuple of t list
    | Discard
end

module Term = struct
  type t =
    | Apply of Restriction.t * t * t
    | Const of Const.t
    | Lambda of Restriction.t * Pattern.t * t
    | Let of Restriction.t * Pattern.t * t * t
    | LetRec of string * t * t
    | Effect of string * t
    | Handler of handler
    | Tuple of t list
    | Var of string
    | Annotated of t * Ty.t
    | Handle of t * t
    | If of t * t * t
    | Variant of string * t option
    | Match of Restriction.t * t * (Pattern.t * t) list
    | Function of (Pattern.t * t) list

  and handler = {
    eff_clauses : (string * Pattern.t * Pattern.t * t) list;
    val_clause : Pattern.t * t;
  }
end
