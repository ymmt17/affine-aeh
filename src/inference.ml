open UntypedSyntax
open Symbol
module Used = Set.Make (Variable)

let ( let* ) = Result.bind

type context = {
  var_context : Ty.scheme VarContext.t;
  var1_context : Ty.scheme VarContext.t;
  eff_context : (Restriction.t * Ty.t * Ty.t) EffContext.t;
  tyname_context : (Parameter.t list * TyDef.t) TyNameContext.t;
  constr_context : (TyName.t * Parameter.t list * Restriction.t * Ty.t option) LabelContext.t;
}

let new_context =
  {
    var_context = Predefined.var_ty_context;
    var1_context = VarContext.empty;
    eff_context = Predefined.eff_ty_context;
    tyname_context = Predefined.tydef_context;
    constr_context = Predefined.constr_ty_context;
  }

type state = { constraints : (Ty.t * Ty.t) list }

let new_state = { constraints = [] }

let new_param_ty () = Ty.Param (Parameter.make ())

let add_constraint state ty1 ty2 = { constraints = (ty1, ty2) :: state.constraints }

let add_effect ctx eff restr ty1 ty2 =
  let eff_context = EffContext.add eff (restr, ty1, ty2) ctx.eff_context in
  { ctx with eff_context }

let add_tydef ctx params tyname = function
  | TyDef.Alias _ as def ->
      let tyname_context = TyNameContext.add tyname (params, def) ctx.tyname_context in
      { ctx with tyname_context }
  | TyDef.Variant constrs as def ->
      let constr_context =
        LabelContext.fold
          (fun label (restr, tyopt) context ->
            LabelContext.add label (tyname, params, restr, tyopt) context)
          constrs ctx.constr_context
      in
      let tyname_context = TyNameContext.add tyname (params, def) ctx.tyname_context in
      { ctx with constr_context; tyname_context }

let extend_poly bind ctx =
  ListLabels.fold_left bind ~init:ctx ~f:(fun ctx (x, restr, tysch) ->
      match restr with
      | `Unrestricted -> { ctx with var_context = VarContext.add x tysch ctx.var_context }
      | `AtMostOnce -> { ctx with var1_context = VarContext.add x tysch ctx.var1_context })

let extend bind = extend_poly (List.map (fun (x, restr, ty) -> (x, restr, Ty.Scheme ([], ty))) bind)

let vacate ctx = { ctx with var1_context = VarContext.empty }

module Subst = Map.Make (Parameter)

let rec subst_ty subst = function
  | Ty.Basic _ as ty -> ty
  | Ty.Param p as ty -> Option.value ~default:ty (Subst.find_opt p subst)
  | Ty.Apply (tys, name) -> Apply (List.map (subst_ty subst) tys, name)
  | Ty.Arrow (restr, ty1, ty2) -> Arrow (restr, subst_ty subst ty1, subst_ty subst ty2)
  | Ty.Handler (ty1, ty2) -> Handler (subst_ty subst ty1, subst_ty subst ty2)
  | Ty.Tuple tys -> Tuple (List.map (subst_ty subst) tys)

let subst_tydef subst = function
  | TyDef.Alias ty -> TyDef.Alias (subst_ty subst ty)
  | TyDef.Variant constrs ->
      let constrs =
        LabelContext.map (fun (restr, tyopt) -> (restr, Option.map (subst_ty subst) tyopt)) constrs
      in
      TyDef.Variant constrs

let subst_context ctx subst =
  let var_context =
    VarContext.map
      (fun (Ty.Scheme (params, ty)) -> Ty.Scheme (params, subst_ty subst ty))
      ctx.var_context
  in
  { ctx with var_context }

let add_subst p ty subst = subst |> Subst.map (subst_ty (Subst.singleton p ty)) |> Subst.add p ty

let rec unify ctx ty1 ty2 subst =
  let ty1 = subst_ty subst ty1 and ty2 = subst_ty subst ty2 in
  match (ty1, ty2) with
  | ty1, ty2 when ty1 = ty2 -> Ok subst
  | Ty.Param p, ty | ty, Ty.Param p ->
      if Ty.occurs_in p ty then Error `CyclicType else Ok (add_subst p ty subst)
  | Ty.Apply (tysl, namel), Ty.Apply (tysr, namer)
    when namel = namer && List.length tysl = List.length tysr ->
      ListLabels.fold_left2 tysl tysr ~init:(Ok subst) ~f:(fun acc ty1 ty2 ->
          Result.bind acc (unify ctx ty1 ty2))
  | Ty.Apply (tys, name), ty | ty, Ty.Apply (tys, name) -> unify_defined_ty ctx tys name ty subst
  | Ty.Arrow (restr1, ty1l, ty1r), Ty.Arrow (restr2, ty2l, ty2r) when restr1 = restr2 ->
      let* subst = unify ctx ty1l ty2l subst in
      unify ctx ty1r ty2r subst
  | Ty.Handler (ty1l, ty1r), Ty.Handler (ty2l, ty2r) ->
      let* subst = unify ctx ty1l ty2l subst in
      unify ctx ty1r ty2r subst
  | Ty.Tuple tysl, Ty.Tuple tysr when List.(length tysl = length tysr) ->
      ListLabels.fold_left2 tysl tysr ~init:(Ok subst) ~f:(fun acc ty1 ty2 ->
          Result.bind acc (unify ctx ty1 ty2))
  | ty1, ty2 -> Error (`UnificationFailed (ty1, ty2))

and unify_defined_ty ctx tys name ty subst =
  let* tydef = applied_tydef ctx tys name in
  match tydef with
  | TyDef.Alias ty' -> unify ctx ty ty' subst
  | _ -> Error (`UnificationFailed (Ty.Apply (tys, name), ty))

and applied_tydef ctx tys name =
  let params, tydef = TyNameContext.find name ctx.tyname_context in
  let nparams = List.length params and nargs = List.length tys in
  if nparams <> nargs then Error (`TyConstrArgsMismatch (nparams, nargs))
  else
    let subst = List.fold_right2 Subst.add params tys Subst.empty in
    Ok (subst_tydef subst tydef)

let solve state ctx =
  List.fold_left
    (fun subst (ty1, ty2) -> Result.bind subst (unify ctx ty1 ty2))
    (Ok Subst.empty) state.constraints

let refresh_params state params =
  ListLabels.fold_right params ~init:([], Subst.empty, state) ~f:(fun p (ps, subst, state) ->
      let p' = Parameter.make () in
      let subst = add_subst p (Ty.Param p') subst in
      (p' :: ps, subst, state))

let refresh_ty_scheme state (Ty.Scheme (params, ty)) =
  let params, subst, state = refresh_params state params in
  (Ty.Scheme (params, subst_ty subst ty), state)

let free_params ctx =
  let collect ctx =
    VarContext.fold
      (fun _ (Ty.Scheme (ps, ty)) acc ->
        let ps' = Ty.free_params ty in
        List.filter (fun p -> List.mem p ps) ps' :: acc)
      ctx
  in
  collect ctx.var1_context (collect ctx.var_context []) |> List.flatten

let generalize ctx is_restricted ty =
  if is_restricted then Ty.Scheme ([], ty)
  else
    let ctx_ps = free_params ctx in
    let ps = List.filter (fun p -> not (List.mem p ctx_ps)) (Ty.free_params ty) in
    Ty.Scheme (ps, ty)

let is_expansive = function
  | Computation.Value _ -> false
  | _ -> true

(* let rec infer_pat state ctx m = function
  | Pattern.Const cst -> Ok (Ty.Basic (Const.infer_ty cst), [], state)
  | Pattern.Var x ->
      let ty = new_param_ty () in
      Ok (ty, [ (x, ty) ], state)
  | Pattern.Variant (label, popt) -> (
      let tyname, params, m', tyopt = LabelContext.find label ctx.constr_context in
      if m = `Unrestricted && m' = `AtMostOnce then Error `InconsistentMode
      else
        let params, subst, state = refresh_params state params in
        let ty = Ty.Apply (List.map (fun p -> Ty.Param p) params, tyname) in
        match (tyopt, popt) with
        | None, None -> Ok (ty, [], state)
        | Some ty', Some p ->
            let ty' = subst_ty subst ty' in
            let* ty'', bind, state = infer_pat state ctx m p in
            let state = add_constraint state ty' ty'' in
            Ok (ty, bind, state)
        | _ -> assert false )
  | Pattern.Tuple ps ->
      let* tys, binds, state =
        ListLabels.fold_right ps
          ~init:(Ok ([], [], state))
          ~f:(fun p acc ->
            let* tys, binds, state = acc in
            let* ty, bind, state = infer_pat state ctx m p in
            Ok (ty :: tys, bind :: binds, state))
      in
      Ok (Ty.Tuple tys, List.flatten binds, state) *)

(* let fold m m' =
  match (m, m') with
  | `Unk, m | m, `Unk -> m
  | `Unrestricted, `AtMostOnce | `AtMostOnce, `Unrestricted -> `AtMostOnce
  | `Unrestricted, `Unrestricted -> `Unrestricted
  | `AtMostOnce, `AtMostOnce -> `AtMostOnce *)

let rec infer_pat state ctx ~restr = function
  | Pattern.Const cst -> Ok (Ty.Basic (Const.infer_ty cst), [], state)
  | Pattern.Discard -> Ok (new_param_ty (), [], state)
  | Pattern.Var x ->
      let ty = new_param_ty () in
      Ok (ty, [ (x, restr, ty) ], state)
  | Pattern.Variant (label, popt) -> (
      let tyname, params, restr', tyopt = LabelContext.find label ctx.constr_context in
      if restr = `Unrestricted && restr' = `AtMostOnce then Error `InconsistentRestriction
      else
        let params, subst, state = refresh_params state params in
        let ty = Ty.Apply (List.map (fun p -> Ty.Param p) params, tyname) in
        match (tyopt, popt) with
        | None, None -> Ok (ty, [], state)
        | Some ty1, Some p ->
            let ty1 = subst_ty subst ty1 in
            let* ty2, bind, state = infer_pat state ctx ~restr p in
            let state = add_constraint state ty1 ty2 in
            Ok (ty, bind, state)
        | _ -> assert false )
  | Pattern.Tuple ps ->
      let* tys, binds, state =
        ListLabels.fold_right ps
          ~init:(Ok ([], [], state))
          ~f:(fun p acc ->
            let* tys, binds, state = acc in
            let* ty, bind, state = infer_pat state ctx ~restr p in
            Ok (ty :: tys, bind :: binds, state))
      in
      Ok (Ty.Tuple tys, List.flatten binds, state)

let rec infer_expr used state ctx = function
  | Expression.Const cst -> Ok (Ty.Basic (Const.infer_ty cst), used, state)
  | Expression.Var x -> (
      match VarContext.find_opt x ctx.var_context with
      | Some tysch ->
          let Scheme (_, ty), state = refresh_ty_scheme state tysch in
          Ok (ty, used, state)
      | None when Used.mem x used -> Error (`VariableUsedTwice x)
      | None ->
          let* tysch =
            VarContext.find_opt x ctx.var1_context |> Option.to_result ~none:(`VariableNotFound x)
          in
          let Scheme (_, ty), state = refresh_ty_scheme state tysch in
          Ok (ty, Used.add x used, state) )
  | Expression.Annotated (e, ty) ->
      let* ty', used, state = infer_expr used state ctx e in
      let state = add_constraint state ty ty' in
      Ok (ty, used, state)
  | Expression.Variant (label, eopt) -> (
      let tyname, params, restr, tyopt = LabelContext.find label ctx.constr_context in
      let params, subst, state = refresh_params state params in
      let tys = List.map (fun p -> Ty.Param p) params in
      let ty = Ty.Apply (tys, tyname) in
      match (tyopt, eopt) with
      | None, None -> Ok (ty, used, state)
      | Some ty', Some e ->
          let ty' = subst_ty subst ty' in
          let ctx =
            match restr with
            | `Unrestricted -> vacate ctx
            | `AtMostOnce -> ctx
          in
          let* ty'', used, state = infer_expr used state ctx e in
          let state = add_constraint state ty' ty'' in
          Ok (ty, used, state)
      | _ -> assert false )
  | Expression.Lambda (restr, p, c) ->
      let* ty1, bind, state = infer_pat state ctx ~restr p in
      let ctx = extend bind ctx in
      let* ty2, used, state = infer_comp used state ctx c in
      Ok (Ty.Arrow (restr, ty1, ty2), used, state)
  | Expression.Tuple es ->
      let* tys, used, state =
        ListLabels.fold_right es
          ~init:(Ok ([], used, state))
          ~f:(fun e acc ->
            let* tys, used, state = acc in
            let* ty, used, state = infer_expr used state ctx e in
            Ok (ty :: tys, used, state))
      in
      Ok (Ty.Tuple tys, used, state)
  | Expression.Handler { val_clause = pv, cv; eff_clauses } ->
      let ty1 = new_param_ty () in
      let ty2 = new_param_ty () in
      let* ty1', bind, state = infer_pat state ctx ~restr:`AtMostOnce pv in
      let state = add_constraint state ty1 ty1' in
      let* ty2', used, state = infer_comp used state (extend bind ctx) cv in
      let state = add_constraint state ty2 ty2' in
      let* state =
        EffContext.fold
          (fun eff (p, k, c) acc ->
            let* state = acc in
            let restr, ty1eff, ty2eff = EffContext.find eff ctx.eff_context in
            let* ty1', bind1, state = infer_pat state ctx ~restr p in
            let state = add_constraint state ty1eff ty1' in
            let* tycont, bind2, state = infer_pat state ctx ~restr:`AtMostOnce k in
            let state = add_constraint state tycont (Ty.Arrow (`Unrestricted, ty2eff, ty2)) in
            let ctx = ctx |> vacate |> extend bind1 |> extend bind2 in
            (* let ctx =
                 ctx |> vacate
                 |> extend ((k, `AtMostOnce, Ty.Arrow (`Unrestricted, ty2eff, ty2)) :: bind)
               in *)
            let* ty2', _, state = infer_comp used state ctx c in
            let state = add_constraint state ty2 ty2' in
            Ok state)
          eff_clauses (Ok state)
      in
      Ok (Ty.Handler (ty1, ty2), used, state)

and infer_comp used state ctx = function
  | Computation.Value e -> infer_expr used state ctx e
  | Computation.Effect (eff, e) ->
      let restr, ty1, ty2 = EffContext.find eff ctx.eff_context in
      let ctx = if restr = `Unrestricted then vacate ctx else ctx in
      let* ty, used, state = infer_expr used state ctx e in
      let state = add_constraint state ty1 ty in
      Ok (ty2, used, state)
  | Computation.Apply (restr, e1, e2) ->
      let* ty1, used, state = infer_expr used state ctx e1 in
      let ctx = if restr = `Unrestricted then vacate ctx else ctx in
      let* ty2, used, state = infer_expr used state ctx e2 in
      let ty = new_param_ty () in
      let state = add_constraint state ty1 (Ty.Arrow (restr, ty2, ty)) in
      Ok (ty, used, state)
  | Computation.If (cond, thn, els) ->
      let* ty1, used, state = infer_expr used state ctx cond in
      let state = add_constraint state ty1 Ty.bool in
      let* ty2, used1, state = infer_comp used state ctx thn in
      let* ty3, used2, state = infer_comp used state ctx els in
      let state = add_constraint state ty2 ty3 in
      Ok (ty2, Used.union used1 used2, state)
  | Computation.Match (restr, cond, cases) ->
      let ctx' = if restr = `Unrestricted then vacate ctx else ctx in
      let* ty1, used, state = infer_expr used state ctx' cond in
      let ty2 = new_param_ty () in
      let* state =
        ListLabels.fold_left cases ~init:(Ok state) ~f:(fun acc (p, c) ->
            let* state = acc in
            let* ty1', bind, state = infer_pat state ctx ~restr p in
            let ctx = extend bind ctx in
            let state = add_constraint state ty1 ty1' in
            let* ty2', _, state = infer_comp used state ctx c in
            let state = add_constraint state ty2 ty2' in
            Ok state)
      in
      Ok (ty2, used, state)
  | Computation.Let (restr, p, c1, c2) ->
      let ctx' = if restr = `Unrestricted then vacate ctx else ctx in
      let* ty1, used, state = infer_comp used state ctx' c1 in
      let* ty1', bind, state = infer_pat state ctx ~restr p in
      let state = add_constraint state ty1 ty1' in
      let* subst = solve state ctx in
      let bind =
        List.map
          (fun (x, restr, ty) -> (x, restr, subst_ty subst ty |> generalize ctx (is_expansive c1)))
          bind
      in
      let ctx = extend_poly bind ctx in
      infer_comp used state ctx c2
  | Computation.LetRec (x, p, restr, c1, c2) ->
      let ty1 = new_param_ty () in
      let ty2 = new_param_ty () in
      let ty = Ty.Arrow (restr, ty1, ty2) in
      let ctx = extend [ (x, `Unrestricted, ty) ] ctx in
      let* ty1', bind, state = infer_pat state ctx ~restr p in
      let ctx = extend bind ctx in
      let state = add_constraint state ty1 ty1' in
      let* ty2', used, state = infer_comp used state ctx c1 in
      let state = add_constraint state ty2 ty2' in
      infer_comp used state ctx c2
  | Computation.Handle (h, c) ->
      let* ty1, used, state = infer_expr used state ctx h in
      let* ty2, used, state = infer_comp used state ctx c in
      let ty = new_param_ty () in
      let state = add_constraint state ty1 (Ty.Handler (ty2, ty)) in
      Ok (ty, used, state)

let infer_top state ctx c =
  let* ty, _, state = infer_comp Used.empty state ctx c in
  let* subst = solve state ctx in
  let ctx = subst_context ctx subst in
  let ty = subst_ty subst ty |> generalize ctx (is_expansive c) in
  Ok (ty, ctx, state)

let infer_toplevel_let state ctx p c =
  let* ty1, _, state = infer_comp Used.empty state ctx c in
  let* ty2, bind, state = infer_pat state ctx ~restr:`Unrestricted p in
  let state = add_constraint state ty1 ty2 in
  let* subst = solve state ctx in
  let ctx = subst_context ctx subst in
  let bind =
    ListLabels.map bind ~f:(fun (x, m, ty) ->
        (x, m, subst_ty subst ty |> generalize ctx (is_expansive c)))
  in
  let ctx = extend_poly bind ctx in
  Ok (List.map (fun (x, _, tysch) -> (x, tysch)) bind, ctx, state)

let infer_toplevel_letrec state ctx binds =
  let xtys, ctx, state =
    ListLabels.fold_right binds ~init:([], ctx, state)
      ~f:(fun (x, _p, restr, _c) (xtys, ctx, state) ->
        let ty1 = new_param_ty () in
        let ty2 = new_param_ty () in
        let ctx = extend [ (x, `Unrestricted, Ty.Arrow (restr, ty1, ty2)) ] ctx in
        ((x, restr, ty1, ty2) :: xtys, ctx, state))
  in
  let* state =
    ListLabels.fold_left2 xtys binds ~init:(Ok state)
      ~f:(fun acc (_x, _, ty1, ty2) (_, p, restr, c) ->
        let* state = acc in
        let* ty1', bind, state = infer_pat state ctx ~restr p in
        let state = add_constraint state ty1 ty1' in
        let ctx = extend bind ctx in
        let* ty2', _, state = infer_comp Used.empty state ctx c in
        let state = add_constraint state ty2 ty2' in
        Ok state)
  in
  let* subst = solve state ctx in
  let ctx = subst_context ctx subst in
  let tyschs =
    ListLabels.map xtys ~f:(fun (_x, restr, ty1, ty2) ->
        subst_ty subst (Ty.Arrow (restr, ty1, ty2)) |> generalize ctx false)
  in
  Ok (tyschs, ctx, state)

(* let infer_toplevel_letrec state ctx x p restr c =
  let ty1 = new_param_ty () in
  let ty2 = new_param_ty () in
  let ty = Ty.Arrow (restr, ty1, ty2) in
  let ctx = extend [ (x, `Unrestricted, ty) ] ctx in
  let* ty1', bind, state = infer_pat state ctx ~restr p in
  let state = add_constraint state ty1 ty1' in
  let ctx' = extend bind ctx in
  let* ty2', _, state = infer_comp Used.empty state ctx' c in
  let state = add_constraint state ty2 ty2' in
  let* subst = solve state ctx in
  let ctx = subst_context ctx subst in
  Ok (subst_ty subst ty |> generalize ctx false, ctx, state) *)
