open Symbol

type t =
  | Basic of Const.Ty.t
  | Apply of t list * TyName.t
  | Arrow of Restriction.t * t * t
  | Param of Parameter.t
  | Tuple of t list
  | Handler of t * t

type scheme = Scheme of Parameter.t list * t

val int : t

val float : t

val bool : t

val string : t

val unit : t

val occurs_in : Parameter.t -> t -> bool

val free_params : ?ps:Parameter.t list -> t -> Parameter.t list

val print_ty : ?ps:Parameter.t list -> t -> Format.formatter -> unit

val print : scheme -> Format.formatter -> unit
