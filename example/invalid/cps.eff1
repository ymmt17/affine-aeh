type #term =
  | Var of string
  | Lam of string * term
  | App of term * term;;

effect GenSym : string -> string;;

let gensym = handler
  | effect (GenSym s) k -> fun n -> k (s ^ string_of_int n) (n + 1)
  | v -> fun _ -> v;;

effect Shift : ((string -> term) #-> term) #-> string;;

let rec naive_cps_ = fun t #-> match# t with
  | Var x ->
      let k = perform (GenSym "k") in
      Lam (k, App (Var k, Var x))
  | Lam (x, t) ->
      let k = perform (GenSym "k") in
      Lam (k, App (Var k, Lam (x, naive_cps_ # t)))
  | App (t1, t2) ->
      let k = perform (GenSym "k") in
      let k1 = perform (GenSym "k") in
      let k2 = perform (GenSym "k") in
      Lam (k, App (naive_cps_ # t1,
          Lam (k1, App (naive_cps_ # t2,
              App (App (Var k1, Var k2), Var k)))));;

let naive_cps = fun t #-> (with gensym handle naive_cps_ # t) 0;;

let t = Lam ("f", Lam ("x", Lam ("y", App (App (Var "f", Var "y"), Var "x"))));;

naive_cps # t;;

(* let rec cps t k =
  match t with
  | Var x -> k (Var x)
  | Lam (x, t) ->
      let k' = Gensym.make "k" in
      k (Lam (x, Lam (k', cps0 t (Var k'))))
  | App (t1, t2) ->
      let a = Gensym.make "a" in
      cps t1 @@ fun m ->
      cps t2 @@ fun n ->
      App (App (m, n), Lam (a, k (Var a)))

and cps0 t k =
  match t with
  | Var x -> App (k, Var x)
  | Lam (x, t) ->
      let k' = Gensym.make "k" in
      App (k, Lam (x, Lam (k', cps0 t (Var k'))))
  | App (t1, t2) ->
      cps t1 @@ fun m ->
      cps t2 @@ fun n ->
      App (App (m, n), k) *)

